# Liblast
A libre multiplayer FPS game and framework built using Godot game engine and a fully open-source tool chain.

## How to get started

### The manual way

#### 1. Prerequisites

To work with Liblast you will need a minimum set of tools:
- `git` - the ubiquitous free distributed source management system
- `git-lfs` - "Large File Storage" extension for Git. It doesn't always come preinstalled with Git
- `godot` - the `main` branch of Liblast will always try to follow the latest stable release of Godot 4

#### 2. Clone the git repository
Downloading an archive of the source code will not work, because we're using Git LFS to store binary files

```bash
# The HTTPS URL will work for everyone
git clone https://codeberg.org/Liblast/Liblast.git # download the project repository

# Use the SSH URL only if you are a collaborator in the repository
# git clone git@codeberg.org:Liblast/Liblast.git # download the project repository

cd Liblast # enter the repository
git lfs install # ensure Git LFS is set up in the repository
git lfs pull # ensure Git LFS objects are ready
```

#### 3. Open the project in Godot 4
The game project is located in `Liblast/src/game/project.godot`.
Import and open it.

#### 4. Install required Add-ons
After the project imports and opens in the editor, head over to Godot's Asset Library and install the following add-ons:
- Godot Jolt by mihe
- Panku Console by k2kra

#### 5. Install recommended Add-ons
The following add-ons aren't necessary, but might make your life easier:
- Little Camera Preview by anthonyec - helps a lot with adjusting camera positions
- TODO Manager (Godot 4) by OrigamiPete - helps keeping track of actionable comments in the source code
- Project Time Tracker by victormajida - helps track how much time are you spending on the project

#### 6. Reload the project
Godot will ask you to restart the editor after you install each add-on, but you can safely do that once after all add-ons are set up.

#### 7. Run the game
After you reload the project, make sure everything works by hitting `F5` to start the game.
If the game starts and you can join the public server or start a local one - you're all set!

### The automated way (Linux)
You can use a Bash script listed below, that'll do everything listed above for you with no effort. Great for testing stuff.

You do need to have `git`, `git-lfs`, `unzip` and `wget` available on your system.

```bash
#!/bin/bash
# get game code
git clone https://codeberg.org/Liblast/Liblast.git
cd Liblast
git lfs install
git lfs pull
cd ./src/game/
# set Godot version
GODOT_VER="4.2.2-stable"
#GODOT_VER="4.3-beta3"
# download Godot
wget "https://github.com/godotengine/godot-builds/releases/download/$GODOT_VER/Godot_v""$GODOT_VER""_linux.x86_64.zip"
unzip "Godot_v""$GODOT_VER""_linux.x86_64.zip"
rm "Godot_v""$GODOT_VER""_linux.x86_64.zip"
mv "Godot_v""$GODOT_VER""_linux.x86_64" "./godot"
# install required add-ons
cd addons
wget "https://github.com/godot-jolt/godot-jolt/releases/download/v0.12.0-stable/godot-jolt_v0.12.0-stable.zip"
unzip "godot-jolt_v0.12.0-stable.zip"
rm "godot-jolt_v0.12.0-stable.zip"
git clone "https://github.com/Ark2000/PankuConsole.git"
mv -v PankuConsole/addons/panku_console/ .
rm -rf PankuConsole/
cd ..
# import assets
./godot --headless --import
# run game
./godot
```

