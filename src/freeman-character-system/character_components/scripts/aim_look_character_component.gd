extends CharacterComponent
class_name AimLookCharacterComponent

@export_group("Mouse settings")
@export_range(0, 100) var mouse_sensitivity: float = 0.05
@export_group("Joypad settings")
@export var joypad_sensitivity:float = 2

#TODO Set default path once is merged!
@export var joypad_sensitivity_curve: Curve



func _ready():
	await character.ready
	
	var path: String = str(get_tree().current_scene.get_path()) + "/" + str(get_tree().current_scene.get_path_to(self))
	assert(character.head, "AimLookCharacterComponent requires the Character to have a CharacterHead as a child of Character. " + " (Path: " + path + ")" )


func _unhandled_input(event)-> void:
	if Input.mouse_mode != Input.MOUSE_MODE_CAPTURED:
		if event is InputEventKey:
			if event.is_action_pressed("ui_cancel"):
				get_tree().quit()
		 
		if event is InputEventMouseButton:
			if event.button_index == 1:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		
		return
	
	if event is InputEventKey:
		if event.is_action_pressed("ui_cancel"):
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			
		return
	
	if event is InputEventMouseMotion:
		mouse_aim_look(event)


func _process(delta):
	joypad_aim_look(delta)


func mouse_aim_look(event: InputEventMouseMotion)-> void:
	var viewport_transform: Transform2D = get_tree().root.get_final_transform()
	var motion: Vector2 = event.xformed_by(viewport_transform).relative
	
	motion *= mouse_sensitivity
	
	character.add_yaw(motion.x)
	character.add_pitch(-motion.y)
	character.clamp_pitch()


func joypad_aim_look(delta)-> void:
	var joypad_input:Vector2 = Input.get_vector(
	"look_left",
	"look_right",
	"look_down",
	"look_up"
	)
	var joypad_input_lenght:float = joypad_input.length()
	var sensitivity_curve_multiplier:float = joypad_sensitivity_curve.sample(joypad_input_lenght)
	var motion:Vector2 = Vector2(joypad_input.x, joypad_input.y)
	
	motion *= joypad_sensitivity
	motion *= sensitivity_curve_multiplier
	motion *= delta
	
	character.add_yaw(motion.x)
	character.add_pitch(motion.y)
	character.clamp_pitch()
