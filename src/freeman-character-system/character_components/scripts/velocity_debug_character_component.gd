extends CharacterComponent
class_name CharacterDebugCharacterComponent

@export var character_movement_component: MovementCharacterComponent

@onready var state_label: Label = $MarginContainer/MarginContainer/VBoxContainer/State
@onready var total_velocity_label: Label = $MarginContainer/MarginContainer/VBoxContainer/TotalVelocity
@onready var total_speed_label: Label = $MarginContainer/MarginContainer/VBoxContainer/TotalSpeed
@onready var coplanar_velocity_label: Label = $MarginContainer/MarginContainer/VBoxContainer/CoplanarVelocity
@onready var coplanar_speed_label: Label = $MarginContainer/MarginContainer/VBoxContainer/CoplanarSpeed
@onready var is_on_floor_label: Label = $MarginContainer/MarginContainer/VBoxContainer/IsOnFloor
@onready var is_on_wall_label: Label = $MarginContainer/MarginContainer/VBoxContainer/IsOnWall


#TODO Re implement this inheriting the CharacterComponent Scene.
func _ready():
	if !character_movement_component:
		state_label.queue_free()
		state_label = null
	


func _process(_delta):
	if state_label:
		state_label.text = "State: " + character_movement_component.state
	
	total_velocity_label.text = "Total velocity: " + str(character.velocity)
	total_speed_label.text = "Total speed: " + str(character.velocity.length())
	coplanar_velocity_label.text = "coplanar velocity: " + str(character.velocity.slide(character.up_direction))
	coplanar_speed_label.text = "Coplanar speed: " + str(character.velocity.slide(character.up_direction).length())
	is_on_floor_label.text = "Is_on_floor: " + str(character.is_on_floor())
	is_on_wall_label.text = "is_on_wall: " + str(character.is_on_wall())
