extends Node
class_name CharacterComponent

var character: Character

func _ready():
		var path: String = str(get_tree().current_scene.get_path()) + "/" + str(get_tree().current_scene.get_path_to(self))
		assert(character is Character, "CharacterComponent has not been connected. Make sure it is a child of Character and has a CharacterComponentConnector. " + " (Path: " + path + ")" )
