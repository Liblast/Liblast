extends Node
class_name CharacterComponentConnector

func _init():
	await tree_entered
	connect_component()
	
	
func connect_component():
	var component: Node = get_node_or_null(get_parent().get_path())
	var character: Node = get_node_or_null(component.get_parent().get_path())
	var path: String = str(get_tree().current_scene.get_path()) + "/" + str(get_tree().current_scene.get_path_to(self))
	
	assert(component is CharacterComponent, "CharacterComponentConnector must be a child of CharacterComponent." + " (Path: " + path + ")" )
	assert(character is Character, "CharacterComponent must be a child of Character." + " (Path: " + path + ")" )
	
	component.character = character
	
	queue_free()
