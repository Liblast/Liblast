# meta-name: New character controller
# meta-description: Base template for new character controllers.
# meta-default: true

@tool
extends CharacterController

func _process(delta):
	if Engine.is_editor_hint():
		return


func _physics_process(_delta):
	if Engine.is_editor_hint():
		return
	
	move()
