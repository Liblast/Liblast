# meta-name: New character
# meta-description: Base template for new characters.
# meta-default: true

@tool
extends Character

func _process(delta):
	if Engine.is_editor_hint():
		return


func _physics_process(_delta):
	if Engine.is_editor_hint():
		return
