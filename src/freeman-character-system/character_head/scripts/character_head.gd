@tool
extends Node3D
class_name CharacterHead

var character: Character

@export var offset: float = 0.5:
	set = set_offset

func set_offset(value):
	if !character:
		offset = 0
		return
	
	value = clamp(value, 0, character.height)
	offset = value
	
	var desired_position = character.height - offset
	position.y = desired_position


func _init():
	await ready
	check_parent()
	connect("tree_entered", check_parent)


func check_parent():
	var parent: Node = get_parent()
	
	if !parent is Character:
		if character:
			character.head = null
			character = null
		
		update_configuration_warnings()
		return
	
	character = parent
	character.head = self
	set_offset(offset)


func _get_configuration_warnings()-> PackedStringArray:
	if !get_parent() is Character:
		return ["CharacterHead must be a child of Character"]
	
	return []


func _process(_delta):
	protect_position()
	protect_rotation()


func protect_position():
	if !character:
		return
		
	var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
	var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
	var warning: String = "Attempt to move the CharacterHead of '" + character.name + "'" + from + " Position restored."
	var corrected: bool
	
	if !is_zero_approx(position.x):
		position.x = 0
		corrected = true
	
	if !is_zero_approx(position.z):
		position.z = 0
		corrected = true
	
	if !is_equal_approx(position.y, character.height - offset):
		set_offset(offset)
		corrected = true
	
	if !corrected:
		return
		
	push_warning(warning + " (Path: " + path + ")" )

func protect_rotation():
	if !character:
		return
		
	var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
	var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
	var axis: String
	
	var corrected: bool
	
	if !is_zero_approx(rotation.y):
		rotation.y = 0
		axis = " Y"
		corrected = true
	
	if !is_zero_approx(rotation.z):
		rotation.z = 0
		
		axis = " z" if !axis else " y and Z"
		
		corrected = true
	
	if !corrected:
		return
	
	var warning: String = "Attempt to rotate the CharacterHead of '" + character.name + "'" + " on the" + axis + " axis" + from + " Rotation restored."
	push_warning(warning + " (Path: " + path + ")" )
