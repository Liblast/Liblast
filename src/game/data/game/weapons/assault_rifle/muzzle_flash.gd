extends Node3D

var instances: Array[MeshInstance3D]

var tween: Tween

@onready var time_offset : float = randf_range(0, 1)

@export var duration: float = 0.1

var animation_progress: float = 0:
	set(value):
		animation_progress = value
		for i in instances:
			i.set_instance_shader_parameter(&"animation_progress", value)


## Scan the tree under this node to find all mesh instances and add them to the list
func find_meshes(parent: Node):
	for i in parent.get_children():
		if i is MeshInstance3D:
			i.set_instance_shader_parameter("time_offset", time_offset * (instances.size() + 1)) # offset the time between each instance
			instances.append(i)
			if i.get_child_count() > 0:
				find_meshes(i)


func _ready() -> void:

	find_meshes(self)
	animation_progress = 0 # ensure nothing is on
	hide()


func flash():
	if is_instance_valid(tween):
		if tween.is_running():
			tween.kill()

	tween = create_tween()
	show() # show the muzzle flash meshes to play the animation
	var random_offset : float = randf_range(0, 0.3)
	tween.tween_property(self, "animation_progress", 1, duration * (1 - random_offset)).from(random_offset)
	tween.parallel()
	tween.tween_property($OmniLight3D, "light_color", Color(0.5, 0.4, 0.1), duration * (1 - random_offset))\
	.from(Color(1, 0.9, 0.3)).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_EXPO)
	tween.chain()
	tween.tween_property(self, "visible", false, 0) # hide the muzzle flash as soo nas the animation is over
	tween.play()
