extends CharacterBody3D

## PLAYER

var balance = {
	"walk_speed" = 10,
	"walk_damp" = 0.6,
	"jump_vel" = 20,
	"gravity" = 0.1,
	"accel_ground" = 16,
	"accel_air" = 0.3,
	}

var dir : Vector2

func move_player(delta : float, wish_dir : Vector2, jump_active: bool):
	velocity.x *= balance.walk_damp
	velocity.z *= balance.walk_damp

	var vel = Vector3.DOWN * balance.gravity # gravity

	var accel = balance.accel_ground if is_on_floor() else balance.accel_air
	dir = lerp(dir, wish_dir, accel * delta)
	vel.x = dir.x
	vel.z = -dir.y
	velocity += vel * balance.walk_speed # walk

	if jump_active and is_on_floor():
		velocity += Vector3.UP * balance.jump_vel

	move_and_slide()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta: float) -> void:

	var jump_active = Input.is_key_pressed(KEY_SPACE)
	var wish_dir = Input.get_vector("ui_left", "ui_right", "ui_down", "ui_up").normalized()

	move_player(delta, wish_dir, jump_active)

	var ping = %PlayerPing.text as int
	var deviation = %PlayerStdDeviation.text as int

	ping += round(max(randfn(ping, deviation), 0))

	var data = {
		"frame" = Engine.get_physics_frames(),
		"wish_dir" = dir,
		"pos" = position,
		"vel" = velocity,
		"ping" = ping,
		"loss" = %PlayerLoss.text as int,
		"jump" = jump_active,
	}

	send_update_to_server(data)


func send_update_to_server(data : Dictionary):
	if randf() > (data.loss / 100.0):
		get_tree().create_timer(data.ping / 1000.0 / 2.0).timeout.connect(func():\
		%ServerCharacter.recieve_update_from_player(data) )
