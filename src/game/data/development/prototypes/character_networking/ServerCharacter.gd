extends CharacterBody3D

## SERVER

var balance = {
	"walk_speed" = 10,
	"walk_damp" = 0.6,
	"jump_vel" = 20,
	"gravity" = 0.1,
	"accel_ground" = 16,
	"accel_air" = 0.3,
	}

var dir : Vector2

var last_pos_snapped_packet_frame := 0

var last_packet : Dictionary = {
	"frame" = 0,
	"wish_dir" = Vector2.ZERO,
	"pos" = Vector3.ZERO,
	"vel" = Vector3.ZERO,
	"ping" = 0,
	"loss" = 0,
	"jump" = false,
}

var packets = {} # timestamp : data


func move_player(delta : float, wish_dir : Vector2, jump_active: bool):
	velocity.x *= balance.walk_damp
	velocity.z *= balance.walk_damp

	var vel = Vector3.DOWN * balance.gravity # gravity

	var accel = balance.accel_ground if is_on_floor() else balance.accel_air
	dir = lerp(dir, wish_dir, accel * delta)
	vel.x = dir.x
	vel.z = -dir.y
	velocity += vel * balance.walk_speed # walk

	if jump_active and is_on_floor():
		velocity += Vector3.UP * balance.jump_vel

	move_and_slide()


func _physics_process(delta: float) -> void:
	match %ServerMode.current_tab:
		0: # dumb
			return
		1: # active
			if packets.is_empty():
				return

			var data = {}
			var frame = Engine.get_physics_frames()

			while data.is_empty() or frame <= Engine.get_physics_frames() - 240:
				if packets.keys().has(frame):
					data = packets[frame]
				else:
					frame -= 1 # try a packet one frame older
				#print("Most recent packet is for timestamp ", frame)

			# only snap position if sufficiently different from current. Avoid reapllying the same position though
			if position.distance_to(data.pos) > 1.0:
				if data.frame != last_pos_snapped_packet_frame:
					position = data.pos
					last_pos_snapped_packet_frame = data.frame

			velocity = data.vel
			dir = data.wish_dir
			var jump_active = data.jump

			move_player(delta, dir, jump_active)

			# Prepare packet for peer
			data.pos = position
			data.vel = velocity
			data.dir = dir
			data.frame = Engine.get_physics_frames()
			send_update_to_peer(data)


func recieve_update_from_player(data : Dictionary):
	match %ServerMode.current_tab:
		0: # dumb
			if %ServerOrdered.button_pressed:
				if data.frame >= last_packet.frame:
					position = data.pos
			else:
				position = data.pos
			send_update_to_peer(data)
		1: # active
			packets[data.frame] = data
	last_packet = data


func send_update_to_peer(data : Dictionary):
	var new_ping = %PeerPing.text as int
	var deviation = %PeerStdDeviation.text as int
	new_ping += round(max(randfn(new_ping, deviation), 0))
	data.ping = new_ping
	data.loss =  %PeerLoss.text as int
	if randf() > (data.loss / 100.0):
		get_tree().create_timer(data.ping / 1000.0 / 2.0).timeout.connect(func():\
		%PeerCharacter.recieve_update_from_server(data) )
