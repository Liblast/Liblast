extends Camera3D

var previous_position : Vector3


func _ready() -> void:
	previous_position = global_position


func _process(delta: float) -> void:
	if Engine.is_in_physics_frame():
		return
	global_position = previous_position.lerp(get_parent().global_position, delta * 50)
	previous_position = global_position
	global_transform = global_transform.orthonormalized()
