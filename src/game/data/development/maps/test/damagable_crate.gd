extends MeshInstance3D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


 #Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_y(delta * 3.1298)
	rotate_x(delta * 2.421)
	rotate_z(delta * -1.336)



func _on_damagable_cube_damaged(damage_hp):
	Logger.log("Cube was DAMAGED for %d hit points!" % [damage_hp], Logger.MessageType.SUCCESS)

