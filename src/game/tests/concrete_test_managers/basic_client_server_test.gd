extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	$Server.create_server()
	$Client.connect_to_server()

	$Server.multiplayer.peer_connected.connect(success)


func success(_client_id : int):
	var success_result = TestResult.new()
	print("Server recieved a client connection. Test successful")
	get_parent().on_test_completed.emit(success_result)
