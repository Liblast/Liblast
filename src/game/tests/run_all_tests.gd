extends Node

var files = null
var file_index = 0
var test_scene = null

var total_succeeded_tests = 0
var total_tests = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	var dir = DirAccess.open("res://tests/concrete_test_managers/")
	var all_files = dir.get_files()

	files = []
	for i in range(len(all_files)):
		if str(all_files[i]).ends_with("manager.tscn"):
			files.append(dir.get_current_dir() + all_files[i])

	test_next_file()


func test_next_file():
	if len(files) > file_index:
		var current_file = files[file_index]
		file_index += 1
		test_file(current_file)
		return true
	else:
		return false


func test_file(path):
	test_scene = load(path).instantiate()
	test_scene.on_test_completed.connect(process_test_result)
	add_child(test_scene)


func process_test_result(result):
	print(str(result))

	if result.success:
		total_succeeded_tests += 1
	total_tests += 1

	remove_child(test_scene)
	test_scene.queue_free()
	test_scene = null

	if not test_next_file():
		print("All tests complete. " + str(total_succeeded_tests) + "/" + str(total_tests) + " tests succeeded. ")
		if total_succeeded_tests == total_tests:
			get_tree().quit()
		else:
			get_tree().quit(1)
