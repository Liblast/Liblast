class_name ExactFloatOutputExpectation
extends OutputExpectation

@export var value : float


func check_output(output):
	return value == output
