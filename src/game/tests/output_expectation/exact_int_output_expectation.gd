class_name ExactIntOutputExpectation
extends OutputExpectation

@export var value : int


func check_output(output):
	return value == output
