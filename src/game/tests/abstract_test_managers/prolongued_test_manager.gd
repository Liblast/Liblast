extends "res://tests/abstract_test_managers/test_manager.gd"

# Test timeout in seconds
var timeout = 1.0


func add_test_scene(packed_scene):
	get_tree().create_timer(timeout).timeout.connect(_on_timeout)
	var scene = packed_scene.instantiate()
	add_child(scene)
	return scene


func _on_timeout():
	var failure_result = TestResult.new()
	failure_result.success = false
	on_test_completed.emit(failure_result)
