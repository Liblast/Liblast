extends "res://tests/abstract_test_managers/instant_test_manager.gd"

@export var packed_scene : PackedScene

@export var function_call : String
@export var expected_output : OutputExpectation

var scene = null


func run_test():
	scene = packed_scene.instantiate()
	add_child(scene)
