extends "res://tests/abstract_test_managers/prolongued_test_manager.gd"

@export var port = 23681

@export var server_scene_path : PackedScene
@export var client_scene_path : PackedScene

var server = null
var server_peer = _get_server_multiplayer_api()
var client = null
var client_peer = _get_client_multiplayer_api()


func _ready():
	add_server()
	add_client()


func _get_server_multiplayer_api():
	var peer = ENetMultiplayerPeer.new()
	peer.create_server(port)
	return peer


func add_server():
	server = add_test_scene(server_scene_path)
	var path = server.get_path()
	get_tree().set_multiplayer(server_peer, path)


func _get_client_multiplayer_api():
	var peer = ENetMultiplayerPeer.new()
	peer.create_client("127.0.0.1", port)
	return peer


func add_client():
	client = add_test_scene(client_scene_path)
	var path = client.get_path()
	get_tree().set_multiplayer(client_peer, path)
