extends Node

signal on_test_completed(test_result)
var test_finished := false


func _ready():
	on_test_completed.connect(_on_test_finished)


func _on_test_finished(_test_result):
	test_finished = true


func run_test():
	# This is an abstract scene, throw error
	assert(false)
