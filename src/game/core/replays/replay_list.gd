extends ItemList


@export_dir var replay_folder : String


var files = []

func _ready():
	list_contents(replay_folder)
	
func list_contents(path):
	clear()
	var dir = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				add_item(str(file_name))
				files.append(file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")
