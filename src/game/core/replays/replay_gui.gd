extends Control

@export var state_system: StateSystem
@export var replay_name = "replay"

var selected_file: int = 0
var selected_file_name: String = ""

var is_paused = false


func _process(delta: float) -> void:
	if state_system:
		%progress.value = state_system.playback_index
		%scrub.value = state_system.playback_index
		%frame_index.text = str(state_system.playback_index)
	

func _on_scrub_value_changed(value: int) -> void:
	state_system.playback = %play.button_pressed
	state_system.playback_index = value

func finish_recording() -> void:
	if state_system.state_document:
		var path = "res://replays/%s%d.res" % [replay_name, %replays.item_count]
		var result = ResourceSaver.save(state_system.state_document, path, ResourceSaver.FLAG_COMPRESS)
		if result == OK:
			%replays.list_contents(%replays.replay_folder)
		else:
			print("Failed to save replay: %s" % path)


func _on_play_toggled(toggled_on: bool) -> void:
	if state_system:
		state_system.playback = toggled_on  # Sync playback with toggle state
		
		if toggled_on:
			# Optionally handle logic when playback starts, e.g., reset recording if needed
			state_system.recording = false
			%record.button_pressed = false  # Ensure recording is off when playing
			state_system.playback = true
		else:
			state_system.playback = false


func _on_load_pressed() -> void:
	load_selected_replay()

	
func _on_record_toggled(toggled_on: bool) -> void:
	if state_system:
		if toggled_on:
			state_system.playback = false
			state_system.recording = true
			%record.modulate.r += 1
		else:
			state_system.recording = false
			state_system.playback = false
			finish_recording()
			%record.modulate.r -= 1

func _on_replays_item_activated(index: int) -> void:
	selected_file = index
	selected_file_name = %replays.get_item_text(index)
	load_selected_replay()

func load_selected_replay() -> void:
	if selected_file_name != "":
		var replay_path = "res://replays/%s" % selected_file_name
		var new_state_document = load(replay_path)
		if new_state_document and new_state_document is StateStream:
			state_system.state_document = new_state_document
			state_system.playback_index = 0
			state_system.playback_started = false
			state_system.playback = true
			
			var total_states = state_system.state_document.recorded_states.size()
			%progress.max_value = total_states
			%scrub.max_value = total_states
			%frame_count.text = str(total_states)
			%scrub.tick_count = int(total_states/64)
		else:
			print("Failed to load replay: %s" % replay_path)
