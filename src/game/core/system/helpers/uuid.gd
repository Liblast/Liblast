## Generates a 128-bit UUID based on current time, local machine unique ID, a random number and the program elapsed time
class_name UUID
extends Resource

var uuid : PackedByteArray

func _init():
	var rnd = RandomNumberGenerator.new()
	rnd.randomize()
	var uuid : PackedByteArray
	uuid.resize(18)
	uuid.encode_u32(0, str(rnd.randf()).hash())
	uuid.encode_u32(4, str(Time.get_ticks_usec()).hash())
	uuid.encode_u32(8, str(Time.get_date_string_from_system()).hash())
	uuid.encode_u32(12, str(Time.get_unix_time_from_system()).hash())
	uuid.encode_u16(16, OS.get_unique_id().hash())
	self.uuid = uuid


func _to_string() -> String:
	return "%02x%02x%02x%02x-%02x%02x%02x%02x-%02x%02x%02x%02x-%02x%02x%02x%02x-%02x%02x" %\
	[
		uuid[0], uuid[1], uuid[2], uuid[3],
		uuid[4], uuid[5], uuid[6], uuid[7],
		uuid[8], uuid[9], uuid[10], uuid[11],
		uuid[12], uuid[13], uuid[14], uuid[15],
		uuid[16], uuid[17],
	]
