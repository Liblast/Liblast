@tool
class_name DebugIndicator
extends Node3D


@export var label_text : String = "Debugindicator":
	set(value):
		#if value == label_text:
			#return`
		label_text = value
		$Label3D.text = label_text
		
@export var dot_color : Color = Color.WHITE:
	set(value):
		#if value == label_text:
			#return
		dot_color = value
		$Sprite3D.modulate = dot_color

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
