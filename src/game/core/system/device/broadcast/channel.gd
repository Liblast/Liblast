# for chat communication
# can be global or local in Game, or exist on/inside a 3D object
# radios can be connected  to multiple channels 
# a character can be connected to multiple channels 
# for example team, or everyone channels for a game.
# some channels can be exclusive, so only certain characters are allowed to join
extends Resource
class_name RadioChannel

@export var channel_name = ""
@export var channel_id = 0

var broadcast_log : Array[String]
