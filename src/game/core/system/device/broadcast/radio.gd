# for chat communication
# maps/game_modes will have a global radio device to connect users to the global chat channel
# each team will have a radio channel

# a user will have a radio and send/receive messages 
# on the channels they are currently tuned into
# when a user sends a message, it gets added to the channel's broadcast log

# there could be some inventive game modes that use radio channels
# as part of the gameplay, like a police force and robbers
# or as part of the environment, like a radio station


class_name Radio
extends Device

@export var connected_channels : Array[RadioChannel]
