class_name ComputerOS
extends Node

var computer : Computer
var current_screen : Control

func show_interface():
	# Create the main OS interface
	var interface = preload("res://core/system/device/computer/os/computer_os.tscn").instantiate()
	current_screen = interface

	# Update all displays
	if computer and computer.displays:
		for display in computer.displays:
			display.update(current_screen)

func get_current_screen() -> Control:
	return current_screen

func process_input(input : String):
	# Process user input here
	pass

func track_device(device : Node):
	# Add logic to track and manage devices in the 3D environment
	pass
