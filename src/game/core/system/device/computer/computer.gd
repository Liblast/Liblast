class_name Computer
extends Device

@export var powered := false

@export var displays : Array[Display]
@export var application : PackedScene #the scene to play
@export var feeds : Array[Display]

var computer_os : ComputerOS

func _ready():
	computer_os = ComputerOS.new()
	computer_os.computer = self
	add_child(computer_os)

func interact(who : Character):
	computer_os.show_interface()

func update_display():
	for display in displays:
		display.update(computer_os.get_current_screen())

func process_input(input : String):
	computer_os.process_input(input)
