extends Label




func _ready():
	# infinite scrolling text
	var tween = create_tween()
	tween.set_loops(-1)
	tween.tween_property(self, "visible_ratio", 0, 1.0)
	tween.tween_property(self, "visible_ratio", 1, 1.0)
	tween.tween_property(self, "visible_ratio", 1, 1.0)
		
