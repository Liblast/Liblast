## A plane that has a 2D viewport
class_name Display
extends MeshInstance3D

@export var size: float = 1.0  # Size of the display, controls the aspect ratio

@export var viewport : SubViewport
var viewport_texture: ViewportTexture

func _ready():
	create_viewport_texture()

func create_viewport_texture():
	await get_tree().process_frame  # Ensure the viewport is ready
	viewport_texture = $SubViewport.get_texture()

func update(content: Control):
	if $SubViewport.get_child_count() > 0:
		$SubViewport.remove_child($SubViewport.get_child(0))
	$SubViewport.add_child(content.duplicate())
