## Handles map, game state, network
class_name Game # session? game is underneath?
extends Node

var services : Dictionary

enum Role {SINGLE, CLIENT, SERVER, DEDICATED_SERVER}

const DEFAULT_PORT = 30012 # unfa subs on YT at 2024-08-25 19:07 CET

var multiplayer_peer : MultiplayerPeer # default should be OfflineMultiplayerPeer
var role : Role

var map_path : String = ""
var map : Map

var game_state : Dictionary
var players : Dictionary

var characters_root : Node


func load_and_spawn_map():
	map = load(map_path).instantiate()
	map.name = "Map"
	add_child(map)


func spawn_character(owner_pid: int = 1) -> Character:
	if not is_node_ready():
		await ready

	var char = preload("res://core/system/entity/character/character.tscn").instantiate()
	char.global_transform = map.get_spawn_transform()
	char.services = services

	return char


func _init(role: Role, params: Dictionary):
	match role:
		Role.SINGLE:
			multiplayer_peer = OfflineMultiplayerPeer.new()
			map_path = params.map
			load_and_spawn_map()
		Role.CLIENT:
			multiplayer_peer = ENetMultiplayerPeer.new()
			multiplayer_peer.create_client(params.address, params.port)
		Role.SERVER:
			multiplayer_peer = ENetMultiplayerPeer.new()
			multiplayer_peer.create_server(params.port)
			load_and_spawn_map()
		Role.DEDICATED_SERVER:
			multiplayer_peer = ENetMultiplayerPeer.new()
			load_and_spawn_map()
		_:
			assert(false, "Game created with unknown role")


	var characters_root = Node.new()
	characters_root.name = "Characters"
	add_child(characters_root)
