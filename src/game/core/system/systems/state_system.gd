class_name StateSystem
extends Node

var world_state: Dictionary = {
	"tick": 0,
	"entity_states": {},
}

@export var recording = true
@export var playback = false
@export var state_stream: StateStream

var playback_index: int = 0  # Track current playback position
var last_recorded_frame: int = -1  # Track the last frame a snapshot was recorded
var playback_started: bool = false

func _ready() -> void:
	if state_stream == null:
		state_stream = StateStream.new()


## I dont like how this is coupled with playback/replays
func _physics_process(delta: float) -> void:
	if recording:
		record_current_frame()

	if playback:
		if not playback_started:
			playback_index = 0
			playback_started = true

		if playback_index < state_stream.recorded_states.size():
			restore_snapshot(state_stream.recorded_states[playback_index])
			playback_index += 1
		else:
			playback = false 


## also unecessary ?
func record_current_frame() -> void:
	var current_frame = Engine.get_physics_frames()
	if last_recorded_frame != current_frame:
		state_stream.recorded_states.append(create_snapshot())
		last_recorded_frame = current_frame

## await..
func create_snapshot() -> Dictionary:
	world_state.tick = Engine.get_physics_frames()
	for i in get_tree().get_nodes_in_group("state_component"):
		world_state["entity_states"][i.get_path()] = i.get_state_packed()
	return world_state.duplicate(true)

func restore_snapshot(state: Dictionary) -> void:
	for path in state["entity_states"].keys():
		var node = get_tree().root.get_node(path)
		if node:
			node.set_state_packed(state["entity_states"][path])
	
	
	print("The world state #%s is %d bytes in size." % [state["tick"], var_to_bytes(state).size()])

	
	
	
	## this loop is for restoring rigidbodies in game for visual consistency.
	## basically inside this loop we restore_snapshot() for n times to get rigidbody to behave
	## had to move it down here sorry
	#var j = 4
	#while j > 0:
	#	j -= 1
