class_name ContentMetadata
extends Resource

@export var content_uuid : PackedByteArray
@export var content_sha256 : PackedByteArray
@export var preview_image : Image ## 
@export var authors : PackedStringArray ## list of authors and contributors
@export var license : String ## simple text multiline
@export var description : String ## RichText, multiline
