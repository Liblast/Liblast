class_name Interaction
extends RayCast3D

@export var material : StandardMaterial3D
@export var character : Character
var current_target : Node3D

func _ready():
	character.use_pressed.connect(start_interaction)
	character.use_released.connect(finish_interaction)


func start_interaction():
	print("Checking interaction")
	if is_colliding():
		print("Colliding with: ", get_collider())
		current_target = get_collider()
		if current_target.has_method("interact"):
			print("Interacting with: ", current_target)
			current_target.interact(character)
			
			for node in current_target.get_children():
				if node is MeshInstance3D:
					node.mesh
		else:
			print("Target does not have interact method")
	else:
		print("Not colliding with anything")


func finish_interaction():
	pass
