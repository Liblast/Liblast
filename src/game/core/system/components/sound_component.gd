class_name AudioComponent
extends AudioStreamPlayer3D
@export var streams : Array[AudioStream]
var target : Node3D




func _ready():
	target = get_node_or_null(get_parent().get_path())
	if target is Mover:
		target.connect("start_reached", start)
		target.connect("target_reached", finished)

func start():
	if streams.is_empty():
		return
	stream = streams[0]
	play()

func finished():
	stream = streams[1]
	play()
	pass
