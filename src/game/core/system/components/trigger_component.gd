class_name Trigger
extends EntityComponent

@onready var debug_indicator : DebugIndicator = get_node_or_null("DebugIndicator")

var parent_zone : Zone


@export var active : bool = false:
	set(value):
		active = value
		#print("Trigger state ", active)
		
		for i in target_entities:
			i.set(&"active", active)
		
		if not debug_indicator:
			return
		debug_indicator.label_text = "Trigger active: %s" % str(active)
		if active:
			debug_indicator.dot_color = Color.GREEN
		else:
			debug_indicator.dot_color = Color.RED


@export var target_entities : Array[Node3D]
func activate(_body):
	if active:
		return

	if parent_zone.bodies_count > 0:
		active = true


func deactivate(_body):
	if parent_zone.bodies_count == 0:
		if active:
			active = false


func _ready():
	if not debug_indicator:
		return
	debug_indicator.global_position = get_parent().global_position + Vector3.DOWN * 1.0
	debug_indicator.label_text = "Trigger"
	
	parent_zone = get_parent() as Zone
	if not parent_zone:
		return
	
	parent_zone.body_entered.connect(activate)
	parent_zone.body_exited.connect(deactivate)
