@tool
extends Button


@export var button_size : Vector2i = Vector2i(32, 32) : set = resize_button
@onready var viewport := get_node("%viewport")
@onready var mesh := get_node("%mesh")
func _ready():
	resized.connect(resize_button(button_size))

func resize_button(new_size):
	button_size = new_size
	size = button_size
	viewport.size = button_size

func _process(delta):
	mesh.rotation.y += 0.01
	
