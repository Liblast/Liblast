extends CoreService
class_name GUI

var game : Game : set = game_changed
var mouse_captured = false

func _input(event):
	# Check if the "ui_cancel" action is pressed to toggle mouse capture.
	if Input.is_action_just_pressed("ui_cancel"):
		if mouse_captured:
			mouse_captured = false
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		else:
			mouse_captured = true
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func game_changed(new_game: Game):
	if new_game:
		print("hey")
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		var pop = PopupMenu.new()
		pop.name = new_game.map.name
		pop.add_item("Leave game")
		pop.connect("index_pressed", leave_game)
		%menu.add_child(pop)
	else:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func leave_game(selection):
	if selection == 0:
		game.queue_free()

func _ready() -> void:
	if services.size() <= 1:
		print("No core services available")
		return

func _on_play_index_pressed(selection):
	if selection == 0:
		services.Commands.play("res://core/data/maps/test_map.tscn")

func _on_user_index_pressed(selection):
	if selection == 0:
		%CharacterSetup.show()
	if selection == 1:
		%CrosshairSetup.show()
