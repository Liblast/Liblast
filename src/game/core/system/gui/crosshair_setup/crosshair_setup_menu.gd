extends Control

@export var crosshair : Node

func _on_draw_cross_toggled(toggled_on):
	crosshair.show_cross = toggled_on

func _on_cross_size_value_changed(value):
	crosshair.crosshair_size = value

func _on_draw_circle_toggled(toggled_on):
	crosshair.show_circle = toggled_on

func _on_circle_size_value_changed(value):
	crosshair.circle_size = value
