
extends CenterContainer

@onready var draw_node = get_child(0)

@export_range(2, 80) var crosshair_size : int = 8 : set = redraw
@export_range(2, 80) var circle_size : int = 8 : set = redraw
@export_range(0.0, 1.0) var alpha : float = 1.0 : set = mod
@export var show_cross : bool = true : set = redraw
@export var show_circle : bool = false : set = redraw
@export var cross_color : Color = Color.WHITE : set = redraw
@export var circle_color : Color = Color.WHITE : set = redraw

func _ready():
	resized.connect(redraw.bind(null))

	# the main window maximising breaks the draw
	draw_node.connect("size_changed", redraw)
func mod(new_alpha):
	alpha = new_alpha
	modulate.a = new_alpha

func redraw(_value):
	queue_redraw()

func _draw():
	#print(Engine.get_frames_drawn())
	if show_cross:
		draw_line(draw_node.position + Vector2(0, -crosshair_size - 1), draw_node.position + Vector2(0, crosshair_size), cross_color, 1.0)
		draw_line(draw_node.position + Vector2(-crosshair_size - 1, 0), draw_node.position + Vector2(crosshair_size, 0), cross_color, 1.0)
	if show_circle:
		draw_circle(draw_node.position, circle_size, circle_color)
