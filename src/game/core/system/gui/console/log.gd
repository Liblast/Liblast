extends RichTextLabel
class_name LogGUI


signal content_updated(bbcode:String)

@export var log : NativeLog

const MAX_LOGS = 128


@export var console_input:LineEdit

func _ready():
	log.info_msg_received.connect(push_content)
	#log.error_msg_received.connect(push_content)
	#log.warning_msg_received.connect(push_content)

func push_content(data : String):
		text += data
		text += "\n"
