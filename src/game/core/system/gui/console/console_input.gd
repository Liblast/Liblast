extends LineEdit


func _on_text_submitted(new_text):
	for command in Commands.console_commands:
		if new_text == command:
			Commands.run_command(new_text)
	text = ""
	
