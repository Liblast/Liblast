class_name Console
extends CanvasLayer

var list = []

func _ready():
	Commands.add_command("clear", clear)


func clear():
	for label in list:
		label.queue_free()
