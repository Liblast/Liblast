extends HBoxContainer

signal color_changed(color : Color)

@export var color : Color : set = update
@export var text : String : set = update

func _ready():
	%Color.color_changed.connect(update)

func update(value):
	if typeof(value) == TYPE_COLOR:
		color = value
		%Color.color = color
		color_changed.emit(color)
	elif typeof(value) == TYPE_STRING:
		text = value
		%Text.text = text
