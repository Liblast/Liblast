extends Control


var rotate_world = false
var rotation_speed = 0.01

##TODO change with animation lol
func _process(delta):
	if rotate_world:
		%World.rotation.y += rotation_speed
##TODO what the feck.
@onready var anim = $HBoxContainer/Control/CenterContainer/ModelView/SubViewport/World/bot01/AnimationPlayer
@onready var body_mesh = $HBoxContainer/Control/CenterContainer/ModelView/SubViewport/World/bot01/bot_01/Skeleton3D/Body

@onready var color_picker_scene = preload("res://core/system/gui/character_setup/color_picker.tscn")


func _ready():
	for animation in anim.get_animation_list():
		%AnimationOption.add_item(animation)

	%AnimationOption.item_selected.connect(animate)
	#%AnimationOption.select(%AnimationOption.item_count-1)

	for body_material in range(body_mesh.mesh.get_surface_count()):
		var color_picker = color_picker_scene.instantiate()
		var mat = body_mesh.mesh.surface_get_material(body_material)
		color_picker.text = mat.resource_name
		color_picker.color = mat.albedo_color
		color_picker.connect("color_changed", change_material_albedo.bind(body_material))
		%Colors.add_child(color_picker)

func change_material_albedo(new_color, body_material):
	var mat = body_mesh.mesh.surface_get_material(body_material)
	mat.albedo_color = new_color


func animate(selected):
	anim.play(%AnimationOption.get_item_text(selected))


func _on_rotating_toggled(toggled_on):
	if toggled_on:
		rotate_world = true
	else:
		rotate_world = false
		%World.rotation = Vector3.ZERO


func _on_rotating_value_changed(value):
	rotation_speed = value


func _on_camera_item_selected(selected):
	if selected == 0:
		%Full.current = true
	if selected == 1:
		%Head.current = true


func _on_animate_toggled(toggled_on):
	pass # Replace with function body.

func _on_color_changed(body_material, new_color):
	var material = body_mesh.mesh.surface_get_material(body_material)
	material.albedo_color = new_color
	body_mesh.mesh.surface_set_material(body_material, material)
