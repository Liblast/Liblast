extends Node3D
class_name Projectile


@export var speed := 10

var shooter : Node3D

func _physics_process(delta):
	position += -transform.basis.z * speed * delta
