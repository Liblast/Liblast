class_name Entity
extends Node

@onready var parent : Node = get_parent()

@export var is_static : bool = false
@export var is_networked : bool = true
@export_category("Networking")
@export_range(0, 64) var networking_priority : int


## current state of the WorldObject
var state : Dictionary


func _init():
	add_to_group("state_component", true)


## State Definition describes what fields does the state dictionary contain, and maps node properties to them
@export var state_definition : Dictionary = {
	"pos" : &"position",
	"rot" : &"rotation",
	"scl" : &"scale",
}



func get_state() -> Dictionary:
	for i in state_definition.keys():
		state[i] = parent.get(state_definition[i] as StringName)
	return state


func set_state(_state: Dictionary):
	#await get_tree().physics_frame
	print("Entity under %s recieved state update %s" % [parent.name, _state])
	for i in state_definition.keys():
		state[i] = _state[i]
		parent[state_definition[i]] = _state[i]


func get_state_packed() -> PackedByteArray:
	var _state = get_state()
	var buf: PackedByteArray = var_to_bytes(_state)
	return buf


func set_state_packed(buf: PackedByteArray):
	var _state : Dictionary = bytes_to_var(buf)
	set_state(_state)


func _process_tick():
	pass
