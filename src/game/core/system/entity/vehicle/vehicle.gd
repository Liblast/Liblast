class_name Vehicle
extends VehicleBody3D

@export var seats : Array[VehicleSeat]
@export var horsepower = 100

var pilot : Character

func _ready():
	for seat in seats:
		if seat.is_pilot:
			seat.seat_entered.connect(on_pilot_entered)
			seat.seat_exited.connect(on_pilot_exited)
		

func on_pilot_entered(character: Character):
	pilot = character
	

func on_pilot_exited(character: Character):
	if character == pilot:
		
		pilot = null
		
func _input(_event):
	if pilot:
		# Forward/Backward control
		if Input.is_action_pressed("move_forward"):
			engine_force = horsepower
		elif Input.is_action_pressed("move_backward"):
			engine_force = -horsepower / 2
		else:
			engine_force = 0.0  # Stop the vehicle when no input

		# Left/Right control with lerping for smooth steering
		if Input.is_action_pressed("move_left"):
			steering = deg_to_rad(25)
		elif Input.is_action_pressed("move_right"):
			steering = deg_to_rad(-25)
		else:
			steering = 0
