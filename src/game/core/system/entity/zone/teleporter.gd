class_name Teleporter
extends Zone


@export_enum("local", "global", "network") var type = 0
## local = this map 
@export var target_local : Teleporter
## global = this server 
#@export var target_global : Node3D
## network = another server 
#@export var target_network : ???
@export var cooldown_timer : Timer


func _init():
	if cooldown_timer == null:
		pass
	else:
		cooldown_timer.connect("timeout", teleport)
	connect("body_entered", teleport)
	#connect("body_exited", teleported)
	

func teleport(body):
	body.position = target_local.position
	
