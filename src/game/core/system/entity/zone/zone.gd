## Zone designates part of the game world to keep track of bodies within and process components on them
## Zone is an area in which Sensor can measure things.
class_name Zone
extends Area3D
@onready var debug_indicator : DebugIndicator = get_node_or_null("DebugIndicator")

@export var affects_characters_only : bool = true

var bodies_count : int = 0:
	set(value):
		bodies_count = value
		
		if not debug_indicator:
			return
		debug_indicator.label_text = "Zone bodies: %d" % [bodies_count]
		if value > 0:
			
			debug_indicator.dot_color = Color.GREEN
		else:
			debug_indicator.dot_color = Color.RED
		
var bodies : Array[PhysicsBody3D]


func _on_body_entered(body: PhysicsBody3D):
	if affects_characters_only and body is not Character:
		return
	bodies.append(body)
	bodies_count += 1


func _on_body_exited(body: PhysicsBody3D):
	if affects_characters_only and body is not Character:
		return
	if bodies.has(body):
		bodies.erase(body)
		bodies_count -= 1
	else:
		push_error("Body %s left a Zone %s but has never entered it?" % [str(body), str(self)])


func _init():
	body_entered.connect(_on_body_entered)
	body_exited.connect(_on_body_exited)
