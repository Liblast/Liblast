class_name Character
extends CharacterBody3D

@export_group("General")
@export var camera := Camera3D
@export var collision : CollisionShape3D
@export var height = 2.0 #m
signal console
signal menu


@export_group("Movement")
@export var movement_speed = 8.0
@export var sprint_factor = 2.0
var sprint_speed : float
@export var jump_force = 10.0
@export var mouse_sensitivity = 0.5
@export var gravity_factor = 2.0


##states

var seated = false # the character is sitting in a Seat


## components

## signals = hotkey / correlates to input map
## add _pressed or _released to auto generate controls!
## could also add _toggle or _pressing for different types.

@export_group("Components")
@export var interaction : Interaction
signal use_pressed
signal use_released

@export var weapon : Weapon
signal fire_pressed
signal fire_released
signal alt_fire_pressed
signal alt_fire_released
signal reload
#signal scope_pressed
#signal scope_released
#signal next_weapon
#signal previous_weapon

@export var radio : Radio
signal chat


var control_signals : Array[String]

func setup_control_signals():
	for sig in get_signal_list():
		if sig["name"] == "input_event":
			break # filter and use only internal signals.
		control_signals.append(sig["name"])

func _init():
	setup_control_signals()

func _ready():
	sprint_speed = movement_speed * sprint_factor




func _input(event):
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		if event is InputEventMouseMotion:
			%Head.rotation_degrees.x = clamp(%Head.rotation_degrees.x - event.relative.y * mouse_sensitivity, -89, 89)
			rotation_degrees.y -= event.relative.x * mouse_sensitivity

	for control in control_signals:
		if control.ends_with("_pressed"):
			if Input.is_action_just_pressed(control.replace("_pressed", "")):
				emit_signal(control)
				print(control)
		elif control.ends_with("_released"):
			if Input.is_action_just_released(control.replace("_released", "")):
				emit_signal(control)
				print(control)
		else:
			if Input.is_action_just_pressed(control):
				emit_signal(control)
				print(control)


func _physics_process(delta: float) -> void:
	# Add the gravity.
	if not is_on_floor():
		velocity += get_gravity() * gravity_factor * delta

	# Handle jump.
	if Input.is_action_pressed("move_jump") and is_on_floor():
		velocity.y = jump_force

	if Input.is_action_pressed("move_sprint"):
		movement_speed = movement_speed + sprint_factor
	if Input.is_action_just_released("move_sprint"):
		movement_speed = movement_speed / sprint_factor

	# Get the input direction and handle the movement/deceleration.
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		var input_dir := Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
		var direction := (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
		if direction:
			velocity.x = direction.x * movement_speed
			velocity.z = direction.z * movement_speed
		else:
			velocity.x = move_toward(velocity.x, 0, movement_speed)
			velocity.z = move_toward(velocity.z, 0, movement_speed)

	# if the character is operating a vehicle then we freeze it in place
	if seated:
		pass # we freeze the character in place
	else:
		move_and_slide()
