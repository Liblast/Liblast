class_name Seat
extends StaticBody3D 

@export var collision_disabled = true


## Signals for entering and exiting the seat.
signal seat_entered(occupant: Character)
signal seat_exited(occupant: Character)

## The character currently in the seat.
var occupant: Character = null
var occupied = false
var occupant_exit_node: Node3D
var occupant_exit_rotation: Vector3
var occupant_exit_view_rotation: Vector3
var occupant_local_exit_position: Vector3
var occupant_local_exit_basis: Basis


func interact(who : Character):
	if occupied:
		pass
	else:
		enter_seat(who)

func enter_seat(who : Character):
	occupant = who
	if occupant: 

		occupant.seated = true
		occupant.collision.disabled = collision_disabled
		occupied = true
		seat_entered.emit(occupant)
		# Store the local exit transform relative to the seat
		var relative_transform = global_transform.inverse() * occupant.global_transform
		occupant_local_exit_position = relative_transform.origin
		occupant_local_exit_basis = relative_transform.basis
		occupant_exit_node = occupant.get_parent_node_3d()
		occupant_exit_rotation = occupant.rotation
		occupant_exit_view_rotation = occupant.camera.rotation
		occupant.global_position = global_position
		occupant.rotation = rotation
		occupant.rotate_y(PI)  # Flip the rotation to face forward
		occupant.set_physics_process(false)
		occupant.reparent(self)
		occupant.use_pressed.connect(exit_seat)
		occupant.interaction.enabled = false


func exit_seat():
	if occupant:
		occupant.seated = false
		occupied = false
		
		occupant.set_physics_process(true)
	
		# Calculate the exit transform in global space
		var exit_transform = global_transform * Transform3D(occupant_local_exit_basis, occupant_local_exit_position)
		
		occupant.reparent(occupant_exit_node)
		
		occupant.global_transform = exit_transform
		if self == VehicleSeat:
			occupant.rotation = Vector3.ZERO
		else:
			occupant.rotation = occupant_exit_rotation
			occupant.camera.rotation = occupant_exit_view_rotation
		seat_exited.emit(occupant)
		occupant.use_pressed.disconnect(exit_seat)
		occupant.interaction.enabled = true
		occupant.collision.disabled = false
		occupant = null
