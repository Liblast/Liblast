class_name Mover
extends Geometry

@onready var debug_indicator : DebugIndicator = get_node_or_null("DebugIndicator")


var move_progress : float = 0.0:
	set(value):
		move_progress = value
		update_debug_indicator_text()
		debug_indicator.dot_color.b = clamp(move_progress, 0, 1)
		position = lerp(start_position, target_position, move_progress)


var start_position : Vector3
var target_position : Vector3
@export var target_offset : Vector3

signal is_active
signal start_reached # reached?
signal target_reached

@export var active : bool = false:
	set(value):
		if value == active:
			return # skip non-updates
		active = value
		
		if not debug_indicator:
			return
		update_debug_indicator_text()
		if active:
			debug_indicator.dot_color = Color.GREEN
		else:
			debug_indicator.dot_color = Color.RED


@export var move_duration_seconds : float = 5.0

func update_debug_indicator_text():
	debug_indicator.label_text = "Mover active: %s, progress: %1.2f" % [str(active), move_progress]


func _ready() -> void:
	start_position = position
	target_position = position + target_offset


func _physics_process(delta: float) -> void:
	if active and move_progress < 1:
		move_progress += delta / move_duration_seconds;
	elif not active and move_progress > 0:
		move_progress -= delta / move_duration_seconds;
	
	if move_progress == 0:
		start_reached.emit() 
	
	if move_progress == 1:
		target_reached.emit()
	
	
	move_progress = clampf(move_progress, 0, 1)
