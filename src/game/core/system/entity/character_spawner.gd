extends Marker3D
class_name CharacterSpawner

@export var spawn_room_size := Vector2.ONE * 4

func _init():
	add_to_group(&"character_spawners", true)


func get_spawn_transform() -> Transform3D:
	## FIXME This will need some collision check later
	return global_transform.translated_local(Vector3(\
			randf_range(-spawn_room_size.x, spawn_room_size.x)/2,\
			randf_range(-spawn_room_size.y, spawn_room_size.y)/2,\
			0.0))
