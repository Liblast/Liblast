## Liblast Framework Core - internal class that acts as a starting point and glue between framework components
class_name Core
extends Node

## Liblast Framework version
const LF_VERSION = {
	&"major" : 0,
	&"minor" : 1,
	&"patch" : 0,
}

@export var services_path = "res://core/system/core/services/"

# or @export var service_paths : GDScript (use inspector)
@export var service_paths : Array = [
	"commands.gd",
	"cvars.gd",
	"logger.gd",
	"pool.gd",
	"stats.gd",
	"storage.gd",
	"user.gd",
]

const LF_LOG_PREFIX: String = "[color=ffdd44]"
const LF_LOG_ERROR_PREFIX: String = "[color=ff4444]"
const LF_LOG_ERROR_STACK_PREFIX: String = "[color=444444]"

var services : Dictionary

## TODO decouple log to Logger?
static func lf_log(message):
	print_rich(LF_LOG_PREFIX, message)


static func lf_log_error(message):
	var stack = get_stack()
	stack.remove_at(0) # remove this level
	var stack_str : String = ""
	for i in stack:
		stack_str += "\n%d: %s:%d @ %s" % [stack.find(i), i.source, i.line, i.function]
	print_rich(LF_LOG_ERROR_PREFIX, message, LF_LOG_ERROR_STACK_PREFIX, stack_str)


func spawn_main_menu():
	var main_menu = load("res://core/system/gui/gui.tscn").instantiate()
	main_menu.name = "MainMenu"
	main_menu.services = services
	add_child(main_menu)


func _init() -> void:
	lf_log("[b]initializing...[/b]")

	var lf_ver : String = "%d.%d.%d" % [
			LF_VERSION.major,
			LF_VERSION.minor,
			LF_VERSION.patch,
			]

	var game_ver = ProjectSettings.get_setting_with_override("application/config/version")
	var game_name = ProjectSettings.get_setting_with_override("application/config/name")

	lf_log("Liblast framework v. %s" % [lf_ver])

	if game_name:
		lf_log("%s" % [game_name])
	if game_ver:
		lf_log("v. %s" % [game_ver])

	lf_log("loading core components...")

	var errors : int = 0

	for i in service_paths:
		var component = load(services_path.path_join(i)) as Script
		if component:
			var component_name = component.get_global_name()
			#lf_log("[color=44ffff]LOAD\t%s" % [i])
			var node = Node.new()
			node.set_script(component)
			node.name = component_name
			add_child(node)
			services[component_name] = node
		else:
			lf_log_error("FAILED to load component from path %s " % [[i]])
			errors += 1

	assert(errors == 0, "%d errors occured while loading core framework components" % [errors])

	# pass service references to other services
	for i in services.keys():
		services[i].services = services

	spawn_main_menu()

	 #pass the core services to all direct children of the core
