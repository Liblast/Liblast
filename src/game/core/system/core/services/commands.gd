## Provides a generalized interface to the framework's core
extends CoreService
class_name Commands

class Command:
	var function : Callable
	var arguments : PackedStringArray
	var required : int
	var description : String
	func _init(in_function : Callable, in_arguments : PackedStringArray, in_required : int = 0, in_description : String = ""):
		function = in_function
		arguments = in_arguments
		required = in_required
		description = in_description

class Cvar:
	##
	pass

static func run_command(input : String):
	# Split the input string into command and arguments
	var input_parts : Array = input.strip_edges().split(" ")
	var command_name : String = input_parts[0]
	var args : Array = input_parts.slice(1, input_parts.size())

	# Check if the command exists in the console_commands dictionary
	if command_name in console_commands:
		var command: Command = console_commands[command_name]

		# Check if the required number of arguments is met
		if args.size() >= command.required:
			# Execute the function with the arguments
			command.function.callv(args)
		else:
			# Handle the case where not enough arguments were provided
			print("Error: The command requires at least %d arguments." % command.required)
	else:
		# Handle the case where the command does not exist
		print("Error: Command '%s' not recognized." % command_name)




func add_base_commands():
	add_command("quit", quit, 0)
	add_command("exit", quit, 0)
	add_command("clear", clear, 0)
	add_command("play", play, 1, 1, "Start a local single-player server and join a map")
	add_command("join", join, 2, 1, "Start a client and join a remote game under a given IP address (and port)")
	add_command("host", host, 2, 1, "Start a server on a specified map")

	#add_command("delete_history", delete_history, 0)
	#add_command("help", help, 0)
	#add_command("commands_list", commands_list, 0)
	#add_command("commands", commands, 0)
	#add_command("calc", calculate, 1)

static var console_commands := {}

func _ready():
	add_base_commands()

static func add_command(command_name : String, function : Callable, arguments = [], required: int = 0, description : String = "") -> void:
	if arguments is int:
		# Legacy call using an argument number
		var param_array : PackedStringArray
		for i in range(arguments):
			param_array.append("arg_" + str(i + 1))
		console_commands[command_name] = Command.new(function, param_array, required, description)

	elif arguments is Array:
		# New array argument system
		var str_args : PackedStringArray
		for argument in arguments:
			str_args.append(str(argument))
		console_commands[command_name] = Command.new(function, str_args, required, description)

static func remove_command(command_name : String) -> void:
	console_commands.erase(command_name)


func quit():
	get_tree().quit()

func clear():
	pass


func join(addr: String = "localhost", port: int = Game.DEFAULT_PORT) -> Game:
	var game = Game.new(Game.Role.CLIENT, {
		"server_addr": addr,
		"port": port,
		})
	game.name = "RemoteGame"
	get_tree().root.add_child(game)
	return game


func play(what: String) -> Game:
	var game = Game.new(Game.Role.SINGLE, {
		"map" : what
		})
	game.name = "SinglePlayerGame"
	get_tree().root.add_child(game)
	return game


func host(server_name: String = "my_game_server", port: int = Game.DEFAULT_PORT) -> Game:
	var game = Game.new(Game.Role.SERVER, {
		"server_name": server_name,
		"port": port,
		})
	game.name = "LocalGame"
	get_tree().root.add_child(game)
	return game
