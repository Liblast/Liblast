## Abstract class for a Liblast Framework Core Component
class_name CoreService
extends Node

var services : Dictionary

#@onready var uuid := UUID.new()


#func _init():
	## TODO Untie this from Core
	#Core.lf_log("[color=4488ff]INIT\t%s" % [self.get_script().get_global_name()])


func _ready() -> void:
	# TODO Untie this from Core
	#Core.lf_log("%s got lf_services dict with %d entries" % [name, services.size()])
	Core.lf_log("[color=44ff88]READY\t%s" % [self.get_script().get_global_name()])
