@tool
extends EditorPlugin

var cvar_editor_menu_item_name := "Liblast Cvar editor..."

var cvar_inspector_plugin : EditorInspectorPlugin


func _enter_tree() -> void:
	add_tool_menu_item(cvar_editor_menu_item_name, open_cvar_editor)
	add_custom_type('Cvar', "Node", load("res://addons/liblast_tools/cvar.gd"), load("res://addons/liblast_tools/cvar_icon.svg"))

	cvar_inspector_plugin = EditorInspectorPlugin.new()
	cvar_inspector_plugin.set_script(load("res://addons/liblast_tools/inspector_plugin_cvar.gd"))
	add_inspector_plugin(cvar_inspector_plugin)


func _exit_tree() -> void:
	remove_tool_menu_item(cvar_editor_menu_item_name)
	remove_custom_type('Cvar')
	remove_inspector_plugin((cvar_inspector_plugin))


func open_cvar_editor() -> void:
	pass
