extends EditorInspectorPlugin

func _can_handle(object: Object) -> bool:
	return true if object is Cvar else false

func _parse_begin(object: Object) -> void:
	print(object)
	#add_property_editor('cvar', load("res://addons/liblast_tools/cvar_inspector_control.tscn").instantiate())
	add_custom_control(load("res://addons/liblast_tools/cvar_inspector_control.tscn").instantiate())
