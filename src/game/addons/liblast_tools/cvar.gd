class_name _Cvar extends Resource

enum CvarType {BOOL, INT, FLOAT, STRING, MAX}

@export var cvar_name : StringName
@export_enum("Boolean", "Integer", "Float", "String") var cvar_type : int
@export var protected : bool
