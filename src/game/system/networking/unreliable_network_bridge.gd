class_name UnrealiableNetworkBridge extends Node

## Provides a fake local network bridge that can be inserted between a client and server to simulate unreliable packet delivery and varying latency

@export var enabled := true

@onready var fake_server_peer := ENetMultiplayerPeer.new() ## Peer that will pretend to be the client
@onready var fake_client_peer := ENetMultiplayerPeer.new() ## Peer that will pretend to be the server

@export var fake_server_port : int = 28928 ## What port should the real client connect to?
@export var fake_client_port : int = 28927 ## What port does the real server listen on?

@export var packet_loss_ratio : float = 0 ## How many packets on average to drop. 0 means no packlet loss, 1 means 100% packet loss
@export var latency_mean_msec : int = 250 ## Mean random packet latency in miliseconds
@export var latency_deviation_msec : int = 50 ## Deviation of the random packet latency in miliseconds

var fake_client_pid : int = -1 ## Cached PID of the remote fake client


func _fake_server_peer_connected(pid):
	fake_client_pid = pid
	fake_client_peer.create_client("localhost", fake_client_port, 1)


func _fake_server_peer_disconnected(_pid):
	fake_client_peer.host.destroy()


func _ready() -> void:
	if not enabled:
		queue_free()
		return

	fake_server_peer.create_server(fake_server_port, 1) # only accept one connection
	fake_server_peer.peer_connected.connect(_fake_server_peer_connected)
	fake_server_peer.peer_disconnected.connect(_fake_server_peer_disconnected)


func _fake_client_put_packet(buf: PackedByteArray, client_pid : int, delay_msec : int):
	fake_client_peer.put_packet(buf)
	print_rich("[i][color=406080]%d → server \t%4d bytes \tlatency %4d ms \tHEX: %s" % [client_pid, buf.size(), delay_msec, buf.hex_encode().left(64).to_upper()])


func _fake_server_put_packet(buf: PackedByteArray, client_pid : int, delay_msec : int):
	fake_server_peer.put_packet(buf)
	print_rich("[i][color=408040]server → %d \t%4d bytes \tlatency %4d ms \tHEX: %s" % [client_pid, buf.size(), delay_msec, buf.hex_encode().left(64).to_upper()])


## Polls the networking peers and relays (or drops) recieved packets
func _process(_delta: float) -> void:
	if not fake_server_peer.get_connection_status() == ENetMultiplayerPeer.CONNECTION_DISCONNECTED:
		fake_server_peer.poll()

	if not fake_client_peer.get_connection_status() == ENetMultiplayerPeer.CONNECTION_DISCONNECTED:
		fake_client_peer.poll()

	if fake_server_peer.get_connection_status() == ENetMultiplayerPeer.CONNECTION_CONNECTED:
		while fake_server_peer.get_available_packet_count() > 0:
			var buf_cl = fake_server_peer.get_packet()
			if randf() > packet_loss_ratio:
				var delay_msec = roundi(randfn(latency_mean_msec, latency_deviation_msec))
				create_tween().tween_interval(delay_msec as float / 1000).finished.connect(\
				func(): _fake_client_put_packet(buf_cl, fake_client_pid, delay_msec))
				#fake_client_peer.put_packet(buf_cl)
			else:
				print_rich("[i][color=406080] %d → server \t%d bytes \t[color=804020]dropped" % [fake_client_pid, buf_cl.size()])

	if fake_client_peer.get_connection_status() == ENetMultiplayerPeer.CONNECTION_CONNECTED:
		while fake_client_peer.get_available_packet_count() > 0:
			var buf_sv = fake_client_peer.get_packet()
			if randf() > packet_loss_ratio:
				var delay_msec = roundi(randfn(latency_mean_msec, latency_deviation_msec))
				create_tween().tween_interval(delay_msec as float / 1000).finished.connect(\
				func(): _fake_server_put_packet(buf_sv, fake_client_pid, delay_msec))

			else:
				print_rich("[i][color=408040]server → %d \t%d bytes \t[color=804020]dropped" % [fake_client_pid, buf_sv.size()])
