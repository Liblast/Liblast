class_name Client extends Node

const SUPPORTED_PROTOCOL_VERSION : int = 1

@export var address : String = "localhost"
@export var port : int = 28927 # unfa's YT subscribers at the time of writing this code
@export var client_name : String = "default"

@export var spatial_origin_position : Vector3

var game_state_scene_path := "res://system/game/game_state.tscn"
var game_state_scene : PackedScene
var game_state : Node
#var game_config : GameConfig

var custom_multiplayer : MultiplayerAPI
var peer : ENetMultiplayerPeer


# lower FPS to save resources when a game client is not actively viewed
#func _notification(what: int) -> void:
	#return
	## only if the client is active. Don't want to affect a server
	#if not is_instance_valid(game_state):
		#return
#
	#match what:
		#NOTIFICATION_APPLICATION_FOCUS_OUT:
			#Engine.max_fps  = 30
		#NOTIFICATION_APPLICATION_FOCUS_IN:
			#Engine.max_fps  = 60


func spawn_client_game_state() -> void:
	game_state_scene = load(game_state_scene_path)
	game_state = game_state_scene.instantiate()
	game_state.is_server = false # we're the client
	game_state.spatial_origin_position = spatial_origin_position
	self.add_child(game_state)


func on_connected():
	print("Connected to server!")
	print("Spawning GameState... ")
	spawn_client_game_state()
	print("OK")
	DisplayServer.window_set_title("CLIENT ID " + str(multiplayer.get_unique_id()))


func on_connection_failed():
	print("Connection to server failed")


func on_server_disconnected():
	print("Disconnected from server!")


@rpc("any_peer", "reliable", "call_local")
func send_message(message):
	if multiplayer.get_remote_sender_id() == multiplayer.get_unique_id():
		print("Sending a message to peers: ", message)
	elif multiplayer.get_remote_sender_id() == 1:
		print("Message from the server: ", message)
	else:
		print("Message from a peer: ", message)
		#create_tween().tween_interval(1)\
		#.finished.connect(func(): send_message.rpc("I got your message, what's up"))


@rpc("authority", "reliable", "call_remote")
func update_game_config(update : Dictionary):
	print("Recieved server's game config update")
	game_state.game_config = dict_to_inst(update)
	game_state.load_map()
	spawn_client_character.rpc_id(1)


## peers use this to request joing the game
@rpc("any_peer", "reliable", "call_local")
func spawn_client_character() -> void:
	print("Client %d requests spawning a character" % [multiplayer.get_unique_id()])
	#if not multiplayer.is_server():
		#return
	#game_state.spawn_character(multiplayer.get_remote_sender_id())


## CLIENT GETS SERVER's QUERY and SENDS REPLY
func _auth_callback(client_id: int, buf : PackedByteArray):
	var dict:Dictionary = bytes_to_var(buf)
	Logger.log(["_auth_callback recieved data ", str(dict), "from PID", client_id, "or", multiplayer.get_remote_sender_id()], Logger.MessageType.QUESTION)

	if dict.protocol != 'liblast':
		Logger.log(["Server protocol mismatch"], Logger.MessageType.ERROR)
		peer.disconnect_peer(1)

	if dict.protocol_version > SUPPORTED_PROTOCOL_VERSION:
		Logger.log(["Server protocol version too new"], Logger.MessageType.ERROR)
		peer.disconnect_peer(1)

	if dict.protocol_version < SUPPORTED_PROTOCOL_VERSION:
		Logger.log(["Server protocol version too old"], Logger.MessageType.ERROR)
		peer.disconnect_peer(1)

	if dict.host_token != 'bruh im legit':
		Logger.log(["Server untrustworthy"], Logger.MessageType.PANIC)
		peer.disconnect_peer(1)

	dict = {
		'client_token' : "me2",
		'client_name' : client_name,
	}

	buf = var_to_bytes(dict)
	multiplayer.send_auth(1, buf)
	multiplayer.complete_auth(1)


func connect_to_server(network_peer := ENetMultiplayerPeer.new()):
	# creating network client
	self.peer = network_peer
	custom_multiplayer = SceneMultiplayer.new()
	get_tree().set_multiplayer(custom_multiplayer, get_path())
	multiplayer.server_relay = false
	multiplayer.connected_to_server.connect(on_connected)
	multiplayer.connection_failed.connect(on_connection_failed)
	multiplayer.server_disconnected.connect(on_server_disconnected)
	multiplayer.auth_callback = _auth_callback
	var err = peer.create_client(address, port)
	multiplayer.multiplayer_peer = peer # custom high-level multiplayer
	if err != OK:
		print("Failed to create network client: ", error_string(err))
		return
	print("Created client!")
