class_name Server extends Node

const SUPPORTED_PROTOCOL_VERSION : int = 1

@export var port : int = 28927 # unfa's YT subscribers at the time of writing this code
@export var server_name : String = "my Liblast server"
@export var game_config : GameConfig

@export var spatial_origin_position : Vector3

var game_state_scene_path := "res://system/game/game_state.tscn"
var game_state_scene : PackedScene
var game_state : GameState
var map_ready := false


var custom_multiplayer : MultiplayerAPI
var peer : ENetMultiplayerPeer


func spawn_server_game_state() -> void:
	game_state_scene = load(game_state_scene_path)
	game_state = game_state_scene.instantiate()
	game_state.is_server = true # we're the server
	game_state.game_config = game_config
	game_state.map_ready.connect(func(): map_ready = true)
	game_state.spatial_origin_position = spatial_origin_position
	self.add_child(game_state)
	game_state.load_map()


## peers use this to request joing the game
@rpc("any_peer", "reliable", "call_remote")
func spawn_client_character() -> void:
	if not multiplayer.is_server():
		return
	game_state.spawn_character(multiplayer.get_remote_sender_id())


## Simplest possible remote function.
@rpc("any_peer", "reliable", "call_local")
func send_message(message):
	if multiplayer.get_remote_sender_id() == 1:
		print("Sending a message to peers: ", message)
	else:
		print("Recieved a message from a peer: ", message)


## SERVER SENDS AUTH QUERY
func on_client_authenticating(client_id : int):
	Logger.log(["Client %d authenticating!" % [client_id]], Logger.MessageType.QUESTION)

	var dict : Dictionary = {
		&'protocol' : "liblast",
		&'protocol_version' : SUPPORTED_PROTOCOL_VERSION,
		&'hostname' : server_name,
		&'host_token' : "bruh im legit",
		}

	var buf:PackedByteArray = var_to_bytes(dict)

	#buf = buf.compress() # Pointless, but... fun?

	multiplayer.send_auth(client_id, buf)


func on_client_connected(client_id : int):
	Logger.log(["Client %d connected!" % [client_id]], Logger.MessageType.INFO)
	send_message.rpc_id(client_id, "Welcome! Your client ID is " + str(client_id))
	if is_instance_valid(game_config):
		# This is where the server tells the newly joined peer what the map is etc
		update_game_config.rpc_id(client_id, inst_to_dict(game_config))
	else:
		push_warning("Server spawned with no game_config. Skipping update to clients")


func on_client_auth_failed(client_id : int):
	Logger.log(["Client %d auth failed!" % [client_id]], Logger.MessageType.ERROR)


## Server uses this to update clients
@rpc("authority", "reliable", "call_remote")
func update_game_config(_update : Dictionary):
	pass # on servers this is a placeholder


func on_client_disconnected(client_id : int):
	Logger.log(["Client %d disconnected!" % [client_id]], Logger.MessageType.INFO)
	game_state.destroy_character(client_id)


func destroy_server():
	peer.host.refuse_new_connections(true)
	for client in peer.host.get_peers():
		client.peer_disconnect_later()
	peer.poll()
	peer.host.flush()
	peer.host.destroy()


## CIENT's REPLY RECIEVED
func _auth_callback(client_id: int, buf : PackedByteArray):
	var dict:Dictionary = bytes_to_var(buf)
	Logger.log(["_auth_callback recieved data ", str(dict), "from PID", client_id, "or", multiplayer.get_remote_sender_id()], Logger.MessageType.QUESTION)
	if multiplayer is SceneMultiplayer:
		if dict.client_token != "me2":
			Logger.log(["Client untrustworthy"], Logger.MessageType.ERROR)
			peer.disconnect_peer(client_id)
			return
		Logger.log(["Client ", dict.client_name, "authenticated"], Logger.MessageType.SUCCESS)
		multiplayer.complete_auth(client_id)

func create_server(network_peer := ENetMultiplayerPeer.new()):
	peer = network_peer
	custom_multiplayer = SceneMultiplayer.new()
	get_tree().set_multiplayer(custom_multiplayer, get_path())
	multiplayer.auth_callback = _auth_callback
	multiplayer.server_relay = false # we don't want the server to mindlessly repeat everything it knows to all peers
	multiplayer.peer_connected.connect(on_client_connected)
	multiplayer.peer_disconnected.connect(on_client_disconnected)
	multiplayer.peer_authenticating.connect(on_client_authenticating)
	multiplayer.peer_authentication_failed.connect(on_client_auth_failed)
	peer.create_server(port)
	multiplayer.multiplayer_peer = peer # custom high-level multiplayer

	Logger.log("Server created", Logger.MessageType.SUCCESS)
	if is_instance_valid(game_config):
		spawn_server_game_state()
	else:
		push_warning("No valid game config provided. Cannot spawn game_state. Unless this is a test, this is incorrect behavior.")
	Logger.log("Spawned GameState", Logger.MessageType.SUCCESS)
	#print("OK")
