extends Node


func _ready() -> void:
	Engine.max_fps = 60


func _on_h_slider_value_changed(value: float) -> void:
	Engine.max_fps = value as int
