## General component that recieves damage and passes that information to whoever wants to listen
## This is what each hitbox on a character should be, but can also be used on damagable props and other game objects
class_name Damagable
extends StaticBody3D

signal damaged(damage_hp: int)

func damage(damage_hp: int):
	print("Damage called on damagable", name, " hp: ", damage_hp)
	damaged.emit(damage_hp)
