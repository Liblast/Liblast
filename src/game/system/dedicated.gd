extends Node

var map_scene_path: String
var server_name: String = "my Liblast server"
var port : int = 28927

func _init() -> void:
	Logger.log("Starting dedicated server")

	var args : PackedStringArray = []
	args = OS.get_cmdline_args()

	if args.has("--map"):
		var arg_idx: int = args.find("--map")

		if args.size() >= arg_idx:
			var path = args[arg_idx +1] # get the following argument
			# clean up the path
			path = path.strip_edges()\
					.strip_escapes()\
					.lstrip('"')\
					.rstrip('"')

			if not path.begins_with("res://data/game/maps/"):
				path = "res://data/game/maps/" + path
			elif not path.begins_with("res://"):
				path = "res://" + path

			if not path.ends_with(".tscn"):
				path += ".tscn"

			map_scene_path = path
			Logger.log(["Requested map:\n", map_scene_path])
	else: # default
		map_scene_path = "res://data/game/maps/city.tscn"
		Logger.log(["No map requested, using default:\n",map_scene_path])

	if args.has("--port"):
		var arg_idx: int = args.find("--port")

		if args.size() >= arg_idx:
			var port = args[arg_idx +1] # get the following argument

	if args.has("--name"):
		var arg_idx: int = args.find("--name")

		if args.size() >= arg_idx:
			server_name = args[arg_idx +1] # get the following argument
			server_name = server_name.strip_edges()\
					.strip_escapes()\
					.lstrip('"')\
					.rstrip('"')


func _ready() -> void:
	%Server.server_name = server_name
	%Server.port = port
	var game_config = GameConfig.new()
	game_config.map_scene_path = map_scene_path
	%Server.game_config = game_config
	%Server.create_server()
	DisplayServer.window_set_title("DEDICATED SERVER (%s)" % [server_name])
