extends CanvasLayer

@export var client : Node
@export var server : Node

var max_fps: int

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	max_fps = Engine.max_fps

	var username
	if OS.has_environment("USER"):
		username = OS.get_environment("USER")
	elif OS.has_environment("USERNAME"):
		username = OS.get_environment("USERNAME")
	else:
		username = "Player"
	%PlayerName.text = username

	var args : PackedStringArray = []
	args = OS.get_cmdline_args()

	print("Found commandline arguments: ",args)

	if args.has("--join"):
		hide()
		var i = args.find("--join")
		var address: String = args[i + 1]
		Logger.log("Found --join commandline argument. Joining server %s..." % [address], Logger.MessageType.INFO)
		client.address = address
		client.client_name = username
		client.connect_to_server()
		DisplayServer.window_set_title("CLIENT")

	#if args.has("--screen"):
		#var i = args.find("--screen")
#
		#var screen_id: int = args[i + 1] as int
		#if screen_id:
			#Logger.log(["Found --screen commandline argument. Setting window's active screen to %s..." % [screen_id]], Logger.MessageType.INFO)
			#DisplayServer.window_set_current_screen(screen_id)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(_delta: float) -> void:
	#pass


func _on_create_server_pressed() -> void:
	hide()
	server.server_name = %ServerName.text
	var game_config = GameConfig.new()
	game_config.map_scene_path = $ServerMenu/VBoxContainer/ServerMapPath.text
	server.game_config = game_config
	server.create_server()

	if not $ServerMenu/HBoxContainer/DedicatedServer.button_pressed:
		client.address = "localhost"
		client.connect_to_server()

	DisplayServer.window_set_title("SERVER")


func _on_create_client_pressed() -> void:
	hide()
	client.address = %ServerAddress.text
	client.client_name = %PlayerName.text
	client.connect_to_server()
	DisplayServer.window_set_title("CLIENT")


func _notification(what: int) -> void:
	match what:
		NOTIFICATION_APPLICATION_FOCUS_OUT:
			Engine.max_fps = 1
		NOTIFICATION_APPLICATION_FOCUS_IN:
			Engine.max_fps = max_fps
