extends Node

func _ready() -> void:
	print("Booting Liblast")

	var args : PackedStringArray = []
	args = OS.get_cmdline_args()

	print("Found commandline arguments: ",args)

	if args.has("--max_fps"):
		var idx = args.find("--max_fps")
		var max_fps: int = args[idx + 1] as int
		if max_fps:
			Logger.log(["Found --max_fps commandline argument. Limiting framerate to %d..." % [max_fps as int]], Logger.MessageType.INFO)
			Engine.max_fps = max_fps

		if args.has("--window_pos"):
			var i = args.find("--window_pos")
			var xy = args[i + 1].split("x", false)
			var vec = Vector2i(xy[0] as int, xy[1] as int)
			print("vec: ", vec)

			var window_pos: Vector2i = vec
			if window_pos:
				Logger.log(["Found --window_pos commandline argument. Setting window position to %s..." % [var_to_str(vec)]], Logger.MessageType.INFO)
				DisplayServer.window_set_position(window_pos)

	if args.has("--window_size"):
		var idx = args.find("--window_size")
		var xy = args[idx + 1].split("x", false)
		var vec = Vector2i(xy[0] as int, xy[1] as int)

		var window_size: Vector2i = vec
		if window_size:
			Logger.log(["Found --window_size commandline argument. Setting window size to %s..." % [var_to_str(vec)]], Logger.MessageType.INFO)
			DisplayServer.window_set_size(window_size)


	if args.has("--dedicated"):
		get_tree().call_deferred(&"change_scene_to_file", "res://system/dedicated.tscn")
	else:
		get_tree().call_deferred(&"change_scene_to_file", "res://system/main.tscn")


