extends Node
class_name Interface

@export var implements: Array[Node]


static func has_interface(node: Node) -> bool:
	for child in node.get_child_count():
		var interface: Interface = node.get_child(child) as Interface

		if interface:
			return true

	return false


static func get_interface(node: Node) -> Variant:
	for child in node.get_child_count():
		var interface: Interface = node.get_child(child) as Interface

		if interface:
			return interface

	return null


static func has_implement(interface: Interface, implement_class: Variant):
	for implement in interface.implements:
		if typeof(implement) == typeof(implement_class):
			return true

	return false
