@tool
extends CharacterController

# TODO Find a more elegant and modular way to expose the character camera
# On the other hand this is an FPS game, can't we assume every character has a first-person camera?
@export var camera : Camera3D

var hud_character_component: HUDCharacterComponent


func _init() -> void:
	super()
	await ready

	var interface = Interface.get_interface(self) as Interface
	for implement in interface.implements:
		if implement is HUDCharacterComponent:
			hud_character_component = implement


func _ready() -> void:
	if not multiplayer.has_multiplayer_peer():
		return

	while not is_instance_valid(multiplayer_character_component):
		await get_tree().process_frame

		# ensure multiplayercomponent is ready
	if not multiplayer_character_component.is_node_ready():
		await multiplayer_character_component.ready

	var color = Color.from_ok_hsl(randf(), 0.96,0.6,1)
	Logger.log("Setting color!")
	Logger.log(["Base Character checks multiplayer_role: ", multiplayer_character_component.MultiplayerRole.keys()[multiplayer_character_component.multiplayer_role]])
	if multiplayer_character_component.multiplayer_role == MultiplayerCharacterComponent.MultiplayerRole.LOCAL:
		#set_accent_color.rpc(color) # Random accent color
		Logger.log("Sending request to change character model color")
#
	#set_accent_color(color)


#@rpc("any_peer", "reliable", "call_local")
#func set_accent_color(color: Color) -> void:
	## FIXME This is bad but temporary. I hope.
	#$ModelCharacterComponent.set_accent_color(color)


func _physics_process(_delta: float) -> void:
	if Engine.is_editor_hint():
		return

	move()
