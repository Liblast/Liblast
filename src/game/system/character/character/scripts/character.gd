@tool
extends CharacterBody3D
class_name Character

##Character class

@export_enum("CapsuleShape3D", "CylinderShape3D") var shape = "CapsuleShape3D":
	set = set_shape

@export var height: float = 2:
	set = set_height

@export var radius: float = 0.5:
	set = set_radius

@export var shape_debug: bool = false:
	set = set_shape_debug

#WARNING Set default paths to character materials once this is merged on main!.
@export_group("Materials (Set default paths!)")
var editor_debug_material: Material = preload("../materials/character_collision_shape_editor_debug.tres")
var debug_material: Material = preload("../materials/character_collision_shape_debug.tres")

const DESTRUCTOR_NAME: String = "CharacterDestructor"
const COLLISION_SHAPE_NAME: String = "CharacterCollisionShape"
const COLLISION_SHAPE_MESH_NAME: String = "CharacterCollisionShapeMesh"

var head: CharacterHead
var character_collision_shape: CharacterCollisionShape
var collision_shape_mesh: MeshInstance3D
var character_destructor: CharacterDestructor

var was_on_floor: bool = false

class CharacterCollisionShape:
	extends CollisionShape3D

	var character: Character

	func _ready():
		character = get_parent()


	func _process(_delta):
		_protect_shape()
		protect_position()
		protect_rotation()
		protect_size()


	func _protect_shape():
		if !shape:
			var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
			var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
			var warning: String = "Attempt to delete the CharacterCollisionShape Shape3D of '" + character.name + "'" + from + " CharacterCollisionShape restored."

			character.set_shape(character.shape)
			push_warning(warning + " (Path: " + path + ")" )


	func protect_position():
		var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
		var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
		var warning: String = "Attempt to move the CharacterCollisionShape of '" + character.name + "'" + from + " Position restored."
		var corrected: bool

		if !is_zero_approx(position.x):
			position.x = 0
			corrected = true

		if !is_zero_approx(position.z):
			position.z = 0
			corrected = true

		if !is_equal_approx(position.y, character.height / 2):
			character.set_vertical_position()
			corrected = true

		if !corrected:
			return

		push_warning(warning + " (Path: " + path + ")" )


	func protect_rotation():
		var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
		var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
		var warning: String = "Attempt to rotate the CharacterCollisionShape of '" + character.name + "'" + from + " Rotation restored."
		var corrected: bool

		if !is_zero_approx(rotation.x):
			rotation.x = 0
			corrected = true

		if !is_zero_approx(rotation.y):
			rotation.y = 0
			corrected = true

		if !is_zero_approx(rotation.z):
			rotation.z = 0
			corrected = true

		if !corrected:
			return

		push_warning(warning + " (Path: " + path + ")" )


	func protect_size():
		var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
		var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
		var warning: String = "Attempt to directly modify the CharacterCollisionShape Shape3D of '" + character.name + "'" + from + " Shape restored."
		var corrected: bool

		if !is_equal_approx(shape.height, character.height):
			character.set_height(character.height)
			corrected = true

		if !is_equal_approx(shape.radius, character.radius):
			character.set_radius(character.radius)
			corrected = true

		if !corrected:
			return

		push_warning(warning + " (Path: " + path + ")" )


class CharacterDestructor:
	extends Node

	func _ready():
		get_parent().script_changed.connect(destroy)

	func destroy():
		if get_parent() is Character:
			return

		get_node("../" + COLLISION_SHAPE_NAME).queue_free()
		get_node("../" + DESTRUCTOR_NAME).queue_free()


func _init():
	character_constructor()


func character_constructor():
	motion_mode = CharacterBody3D.MOTION_MODE_GROUNDED
	create_character_destructor()
	create_character_collision_shape()


func create_character_destructor():
	character_destructor = CharacterDestructor.new()
	character_destructor.name = DESTRUCTOR_NAME
	add_child(character_destructor, true, INTERNAL_MODE_FRONT)


func create_character_collision_shape():
	character_collision_shape = CharacterCollisionShape.new()
	character_collision_shape.name = COLLISION_SHAPE_NAME
	add_child(character_collision_shape, true, INTERNAL_MODE_FRONT)
	character_collision_shape.connect("tree_exited", protect_character)

	collision_shape_mesh = MeshInstance3D.new()
	collision_shape_mesh.name = COLLISION_SHAPE_MESH_NAME
	character_collision_shape.add_child(collision_shape_mesh, true, INTERNAL_MODE_FRONT)

	set_shape(shape)


func protect_character():
	var protected: bool

	if character_collision_shape.is_queued_for_deletion():
		create_character_collision_shape()
		protected = true

	if character_destructor.is_queued_for_deletion():
		create_character_destructor()
		protected = true

	if !protected:
		return

	var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
	var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
	var warning: String = "Attempt to delete internal components of '" + name + "'" + from + " Components restored."

	push_warning(warning + " (Path: " + path + ")" )


func _set(property, value):
	match property:
		"motion_mode":
			if value == MOTION_MODE_GROUNDED:
				return false

			motion_mode = MOTION_MODE_GROUNDED
			var from: String = " from the editor." if Engine.is_editor_hint() else " at runtime."
			var path: String = get_tree().edited_scene_root.scene_file_path if Engine.is_editor_hint() else str(get_tree().current_scene.get_path())
			var warning: String = "Attempt to set the motion mode of '" + name + "' to 'floating'" + from + " Mode unsupported, restoring motion mode."
			push_warning(warning + " (Path: " + path + ")" )
			return true


func set_shape(value):
	value = value if value == "CapsuleShape3D" or value == "CylinderShape3D" else "CapsuleShape3D"
	shape = value

	if !character_collision_shape:
		return

	character_collision_shape.shape = CapsuleShape3D.new() as Shape3D if shape == "CapsuleShape3D" else CylinderShape3D.new() as Shape3D


	collision_shape_mesh.mesh = CapsuleMesh.new() as Mesh if shape == "CapsuleShape3D" else CylinderMesh.new() as Mesh
	collision_shape_mesh.mesh.radial_segments = 24
	collision_shape_mesh.mesh.rings = 3 if shape == "CapsuleShape3D" else 1
	collision_shape_mesh.set("gi_mode", 0)

	set_shape_debug(shape_debug)
	set_height(height)
	set_radius(radius)


func set_height(value):
	value = max(value, 0.001)
	height = value

	if !character_collision_shape:
		return

	var previus_radius: float = character_collision_shape.shape.radius

	character_collision_shape.shape.height = height
	collision_shape_mesh.mesh.height = height

	var current_radius: float = character_collision_shape.shape.radius

	set_vertical_position()

	if head:
		head.set_offset(head.offset)

	if current_radius == previus_radius:
		return

	set_radius(current_radius)


func set_radius(value):
	value = max(value, 0.001)
	radius = value

	if !character_collision_shape:
		return

	collision_shape_mesh.mesh.set("radius", radius)
	collision_shape_mesh.mesh.set("top_radius", radius)
	collision_shape_mesh.mesh.set("bottom_radius", radius)

	var previus_height: float = character_collision_shape.shape.height

	character_collision_shape.shape.radius = radius

	var current_height: float = character_collision_shape.shape.height

	if current_height == previus_height:
		return

	set_height(current_height)


func set_shape_debug(value) -> void:
	shape_debug = value

	if !character_collision_shape:
		return

	character_collision_shape.visible = true if Engine.is_editor_hint() or shape_debug else false
	collision_shape_mesh.mesh.material = debug_material if shape_debug else editor_debug_material


func set_vertical_position() -> void:
	character_collision_shape.position.y = height / 2
