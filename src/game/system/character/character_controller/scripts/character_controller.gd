@tool
extends Character
class_name CharacterController
#TODO: Stair stepping and configurable use real velocity
#CRITICAL: Improve snapping.
#CRITICAL: Use distance to plane to check for the release speed.

#CRITICAL
#TASK:  Report the following bugs:
#BUG:   The real velocity of the body is bigger than the velocity.
#BUG:   Sometimes move_and_slide() moves the body more than its velocity.
#BUG:   get_real_velocity() Sometimes report a bigger velocity than velocity.


@export_group("Snap")
@export var snap_distance: float = 0.2

@export var release_speed: float = 100
@export_group("Stepping")
@export var step_distance: float = 1
@export_group("Clamp settings")
@export var max_pitch: float = 89
@export var min_pitch: float = -89

var multiplayer_character_component: MultiplayerCharacterComponent

const ACTIONS : Array[StringName] = [
	&"look_up",
	&"look_down",
	&"look_left",
	&"look_right",
	&"move_forward",
	&"move_backward",
	&"move_left",
	&"move_right",
	&"jump",
	&"crouch",
	&"hold_crouch",
	&"run",
	&"weapon_trigger_primary",
	&"change_camera",
]

## Translates a local action name to a player-specific action name that is controlled through the multiplayer_character_component. -unfa
func get_action_name(action_name : StringName) -> StringName:
	if multiplayer_character_component: # is this character controller networked?
		# if so - all actions will be parsed through it
		if multiplayer_character_component.ACTIONS.has(action_name):
			return StringName(action_name + "_PID_%d" % [multiplayer_character_component.player_id])
		else: # if it's an action that only matters locally
			return action_name
	else: # if it's single-player only
		return action_name # we use the default actions


# FIXME this
func set_character_visible_color_and_name() -> void:
	var material := StandardMaterial3D.new()
	var character_color: Color
	var character_name: String
	if not multiplayer_character_component:
		character_color = Color.WEB_GRAY
		character_name = "single player character"
	else:
		character_color = Logger._get_color_from_pid(int(str(name)))
		character_name = character_name

	material.albedo_color = character_color
	collision_shape_mesh.set_surface_override_material(0, material)


func _init() -> void:
	super()

	await ready

	var interface = Interface.get_interface(self) as Interface

	for implement in interface.implements:
		if implement is MultiplayerCharacterComponent:
			multiplayer_character_component = implement

	# determine our multiplayer role
	if multiplayer.is_server():
		multiplayer_character_component.multiplayer_role = MultiplayerCharacterComponent.MultiplayerRole.SERVER
	elif int(str(name)) == multiplayer.get_unique_id():
		multiplayer_character_component.multiplayer_role = MultiplayerCharacterComponent.MultiplayerRole.LOCAL
	else:
		multiplayer_character_component.multiplayer_role = MultiplayerCharacterComponent.MultiplayerRole.REMOTE

	Logger.log(["Character", name,"determined it's multiplayer role to be",\
			MultiplayerCharacterComponent.MultiplayerRole.keys()[multiplayer_character_component.multiplayer_role]],\
			Logger.MessageType.INFO)

	# TODO Characetr metadata needs it's own class/resource and path
	set_character_visible_color_and_name()


#WARNING:
#Don't even hope this to work with Godot Physics. Godot Jolt is a must.  Mihe <3
#NOTE: Godot jolt allow to set the iterations and percentage of depenetration
#per iteration. Iterations high, percentage low. I Spend 3 weeks fixing this,
#if we forget about setting jolt in main I will jump from a bridge.
func move()-> void:
	was_on_floor = is_on_floor()

	#HACK:
	#Godot has some stutter issues with collisions. If the velocity is
	##too small, we don't bother moving.
	if velocity.length() < safe_margin:
		return

	#TODO Stair stepping and configurable use real velocity
	var start_position: Vector3 = global_position
	var start_velocity: Vector3 = velocity

	#NOTE:
	#Release the character from the floor if the positive speed along
	#the floor normal is bigger than release_speed.
	#if max(velocity.dot(get_floor_normal()), 0) > release_speed:
		#floor_snap_length = 0

	move_and_slide()

	#HACK:
	#Godot bug. We have to make sure we didn't moved more than the velocity magnitude.
	var real_travel: float = start_position.distance_to(global_position)

	if real_travel > start_velocity.length():
		var difference: float = abs(real_travel - start_velocity.length())
		global_position -= get_last_motion().normalized() * difference

	if !is_on_floor() and was_on_floor:
		apply_floor_snap()

	#FIXME This variable is never used
	var _travel: Vector3 = (global_position - start_position)

	var new_velocity: Vector3 = get_real_velocity()

	#HACK:
	#Godot bug. We have to make sure the new real velocity isn't bigger than the previous one.
	#if new_velocity.length() > start_velocity.length():
		#new_velocity = new_velocity.limit_length(start_velocity.length())
	#
	#HACK:
	#Godot bug. We have to make sure that if we are in the floor plane
	#after moving along the floor plane the velocity remains on the floor plane.
	#NOTICE: My increasingly declining mental health.
	if is_on_floor():
		floor_snap_length = snap_distance

		var floor_plane: Plane = Plane(get_floor_normal())

		#CAUTION:
		#If you mess up with this your whole PC can crash. Division by 0? Try divide by nan.
		if !floor_plane.has_point(new_velocity):
			var lies_over_plane: bool = floor_plane.is_point_over(new_velocity)

			if lies_over_plane:
				new_velocity = floor_plane.intersects_ray(new_velocity, -up_direction)

	#HACK:
	#Only move if the traveled distance is bigger than the collision safe margin.
	#CAUTION: This are a lot of hacks. Pray to Godot for better collisions.
	if global_position.distance_to(start_position) > safe_margin:
		velocity = new_velocity
		return

	#HACK:
	#If we didn't moved enough, reset the position but keep the new velocity.
	#We prefer an small mismatch than a total stop... i think.
	#ATTENTION: please, help.
	global_position = start_position
	velocity = new_velocity


func apply_acceleration(acceleration: Vector3)-> void:
	var process_delta: float = get_process_delta_time()
	var physics_delta: float = get_physics_process_delta_time()
	var delta: float = physics_delta if Engine.is_in_physics_frame() else process_delta

	velocity += acceleration * delta


func apply_impulse(impulse: Vector3)-> void:
	velocity += impulse


func apply_friction(friction: float, min_friction: float = 0, exponent: float = 1, ignore_up_velocity: bool = false)-> void:
	var process_delta: float = get_process_delta_time()
	var physics_delta: float = get_physics_process_delta_time()
	var delta: float = physics_delta if Engine.is_in_physics_frame() else process_delta

	var current_velocity: Vector3 = velocity
	var up_velocity: Vector3 = velocity.project(up_direction)

	current_velocity -= up_velocity if ignore_up_velocity else Vector3.ZERO

	var total_friction: float = friction * pow(current_velocity.length(), exponent)

	total_friction = max(min_friction, total_friction)

	velocity = current_velocity.move_toward(Vector3.ZERO, total_friction * delta)
	velocity += up_velocity if ignore_up_velocity else Vector3.ZERO


## Verifies if we're the server and the RPC being processed came from the character's owner (player)
func check_server_passthrough(remote_sender_id: int) -> bool:
	if multiplayer.is_server() and\
			remote_sender_id == multiplayer_character_component.player_id:
		return true
	else:
		return false


## Ensure only local and server can influence a character
func check_rpc_privilage(remote_sender_id: int) -> bool: #method: Callable) -> bool:
	if multiplayer_character_component: # do we have network at all?
		match remote_sender_id:
			0: # is local call
				#Logger.log(["Allowed", method, "recieved by PID", multiplayer_character_component.player_id, "coming from PID", remote_sender_id], Logger.MessageType.SUCCESS)
				return true
			1: # from server
				#Logger.log(["Allowed", method, "recieved by PID", multiplayer_character_component.player_id, "coming from PID", remote_sender_id], Logger.MessageType.SUCCESS)
				return true
			multiplayer_character_component.player_id: # from owner
				if not multiplayer.is_server(): # only server is allowed to listen to the peer directly
					#Logger.log(["Denied", method, "recieved by PID ", multiplayer_character_component.player_id, "coming from PID", remote_sender_id], Logger.MessageType.WARNING)
					return false
				else:
					#Logger.log(["Allowed", method, "recieved by PID", multiplayer_character_component.player_id, "coming from PID", remote_sender_id], Logger.MessageType.SUCCESS)
					return true
			_: #everyone else:
				#Logger.log(["Denied", method, "for PID", multiplayer_character_component.player_id, "coming from PID", remote_sender_id], Logger.MessageType.WARNING)
				return false
	else: # no network, no problem!
		#Logger.log(["Allowed", method, "as the game is single-player"], Logger.MessageType.SUCCESS)
		return true


## Rotates the character around the local Y axis by a given amount (In degrees) to achieve yaw.
@rpc("any_peer", "call_remote", "reliable")
func add_yaw(amount)-> void:
	if is_zero_approx(amount):
		return

	check_rpc_privilage(multiplayer.get_remote_sender_id()) # this will deny any illegitimate calls to this method

	rotate_object_local(Vector3.DOWN, deg_to_rad(amount))
	orthonormalize()

	# If we're the server and this is processing the call from character owner
	if check_server_passthrough(multiplayer.get_remote_sender_id()):
		add_yaw.rpc_id(-multiplayer.get_remote_sender_id(), amount)


## Rotates the head around the local x axis by a given amount (In degrees) to achieve pitch.
@rpc("any_peer", "call_remote", "reliable")
func add_pitch(amount)-> void:
	if not head:
		#Logger.log(["Can't apply pitch rotation with no head!"], Logger.MessageType.ERROR)
		push_error("Can't apply_pitch with no head!")
		return

	if is_zero_approx(amount):
		return

	check_rpc_privilage(multiplayer.get_remote_sender_id())

	head.rotate_object_local(Vector3.RIGHT, deg_to_rad(amount))
	head.orthonormalize()

	clamp_pitch()


## Sets absolute aim angles (in radians)
@rpc("any_peer", "call_remote", "reliable")
func set_aim_absolute(body_yaw: float, head_pitch : float) -> void:
	if not head:
		#Logger.log(["Can't set_aim_absolute with no head!"], Logger.MessageType.ERROR)
		push_error("Can't set_aim_absolute with no head!")
		return

	check_rpc_privilage(multiplayer.get_remote_sender_id())

	rotation.y = body_yaw
	head.rotation.x = head_pitch

	self.orthonormalize()
	head.orthonormalize()
	#Logger.log(["Set aim absolute", body_yaw, head_pitch], Logger.MessageType.INFO)

	# SERVER PASSTHROUGH
	if check_server_passthrough(multiplayer.get_remote_sender_id()):
		set_aim_absolute.rpc_id(-multiplayer.get_remote_sender_id(), body_yaw, head_pitch)


## Sets absolute (global) position of the character
@rpc("any_peer", "call_remote", "unreliable_ordered")
func set_position_absolute(pos : Vector3) -> void:
	check_rpc_privilage(multiplayer.get_remote_sender_id())
	self.position = pos
	#Logger.log(["Set pos absolute", pos], Logger.MessageType.INFO)

	if check_server_passthrough(multiplayer.get_remote_sender_id()):
		set_position_absolute.rpc_id(-multiplayer.get_remote_sender_id(), pos)


## Clamps the pitch between min_pitch and max_pitch.
func clamp_pitch()-> void:
	if head.rotation.x > deg_to_rad(min_pitch) and head.rotation.x < deg_to_rad(max_pitch):
		return

	head.rotation.x = clamp(head.rotation.x, deg_to_rad(min_pitch), deg_to_rad(max_pitch))
	head.orthonormalize()
