extends CharacterComponent
class_name HUDCharacterComponent

var multiplayer_character_component : MultiplayerCharacterComponent

func _init() -> void:
	await ready

	var interface = Interface.get_interface(character) as Interface

	for implement in interface.implements:
		#if implement is ModelCharacterComponent:
			#model_character_component = implement
		if implement is MultiplayerCharacterComponent:
			multiplayer_character_component = implement
		#elif implement is DamagableCharacterComponent:
			#damagable_character_component = implement

#
#func _on_multiplayer_role_changed(role: MultiplayerCharacterComponent.MultiplayerRole):
	#pass


func _ready():
	if not multiplayer.has_multiplayer_peer():
		return

	while not is_instance_valid(multiplayer_character_component):
		await get_tree().process_frame

	if not multiplayer_character_component.is_node_ready():
		await multiplayer_character_component.ready

	if multiplayer_character_component.is_local_player:
		get_child(0).show() # only child is a canvas layer
	else:
		get_child(0).hide() # only child is a canvas layer
