@tool
extends CharacterComponent
class_name CrouchCharacterComponent
#TODO: Add min crouch size.
#BUG: IF you are crouch, jumo, and uncrouch just before touching the floor you get stuck crouched.
#BUG: Add safe margin to the crouch check. start by checking a little up, then compensate casting a little less.
#BUG: Now character gets stuck under stuff...
#TODO make that you can't crouch more than the radius.
#TODO: If we are crouched and press hold crouch, activate hold crouch

@export_range(1, 10, 0.01, "or_greater", "hide_slider") var crouching_height: float = 1.25:
	set(value):
		crouching_height = value

		if Engine.is_editor_hint():
			return

		if !character:
			return

		if character.shape == "CylinderShape3D":
			return

		value = max(value, character.radius * 2)
@export_range(0, 500, 0.01, "or_greater", "hide_slider") var spring_force: float = 300
@export_range(0, 100, 0.01, "or_greater", "hide_slider") var spring_damping: float = 20

var standing_height: float
var state: StringName = "standing": set = set_state
var can_stand: bool = true
var speed: float
var hold_crouch: bool




func set_state(new_state: StringName):
	if Engine.is_editor_hint():
			return

	if state == new_state:
		return

	var last_state: StringName = state

	exit_state(new_state)
	state = new_state
	enter_state(last_state)

func _ready():
	if Engine.is_editor_hint():
		return

	standing_height = character.height

	if character.shape == "CylinderShape3D":
		return

	crouching_height = max(crouching_height, character.radius * 2)


func _physics_process(_delta)-> void:
	if Engine.is_editor_hint():
		return

	#NOTICE:
	#If we are standing there is no need to check if we can stand.
	if state == "standing" and character.height == standing_height:
		can_stand = true
	else:
		update_can_stand()

	state = get_new_state()

	match state:
		"standing":
			update_height(standing_height)
			return

		"crouched":
			if Input.is_action_just_pressed("crouch"):
				hold_crouch = false

			if Input.is_action_just_pressed("hold_crouch"):
				hold_crouch = true

			update_height(crouching_height)
			return

		"uncrouching":
			if Input.is_action_pressed("crouch"):
				hold_crouch = false

			update_height(standing_height)
			return

		"crouching":
			if Input.is_action_pressed("crouch"):
				hold_crouch = false

			update_height(crouching_height)
			return

		_:
			assert(false, "The state ''" + state + "'' does not exist.")


func get_new_state() -> StringName:
	match state:
		"standing":
			if Input.is_action_just_pressed("crouch"):
				return "crouching"

			if Input.is_action_just_pressed("hold_crouch"):
				hold_crouch = true
				return "crouching"

		"crouched":

			#If we are on the floor and can't stand, we can't do nothing.
			if !can_stand and character.is_on_floor():
				return state

			#If the crouch key is not pressed and hold crouch is false, uncrouch.
			if !Input.is_action_pressed("crouch") and !hold_crouch:
				return "uncrouching"

			#If hold crouch key is  just pressed and hold_crouch is true, uncrouch.
			#If hold_crouch is false, activate hold crouch.
			if Input.is_action_just_pressed("hold_crouch"):
				if hold_crouch:
					return "uncrouching"

			#If the run key is just pressed, uncrouch.
			if Input.is_action_just_pressed("run"):
				return "uncrouching"

		"uncrouching":

			if !can_stand and character.is_on_floor():
				return "crouching"

			if character.height == standing_height:
				return "standing"

			if Input.is_action_just_pressed("crouch"):
				return "crouching"

		"crouching":
			if character.height == crouching_height:
				return "crouched"


			if !Input.is_action_pressed("crouch") and !hold_crouch:
				if can_stand:
					return "uncrouching"

	return state


func enter_state(_last_state: StringName):
	match state:
		"standing":
			hold_crouch = false

		"crouched":
			if !character.is_on_floor():
				return

		_:
			return


func exit_state(_new_state: StringName):
	match state:
		_:
			return


#NOTE This method is only safe to use in physics process.
func update_height(desired_height: float):

	var delta: float = get_physics_process_delta_time()
	var offset: float = character.height - desired_height
	var acceleration: float = -(offset * spring_force) - speed * spring_damping

	var current_character_height: float = character.height

	if is_equal_approx(current_character_height, desired_height) and is_zero_approx(speed):
		return

	speed += acceleration * delta

	var new_height = current_character_height + (speed * delta)

	if new_height < crouching_height:
		new_height = crouching_height
		speed = 0

	if new_height > standing_height:
		new_height = standing_height
		speed = 0

	var height_difference: float = current_character_height - new_height

	#If the character is trying to uncrouch while falling and it would touch the floor,
	#snap the character first.
	if !can_stand and !character.is_on_floor():
		var previous_snap_distance = character.floor_snap_length

		character.floor_snap_length = abs(height_difference) + 0.1 #Small margin.
		character.apply_floor_snap()
		character.floor_snap_length = previous_snap_distance

	character.height = new_height

	if !character.is_on_floor():
		character.position += character.basis.y * height_difference


func update_can_stand():
	var margin: float = character.safe_margin

	var space_needed: float = (standing_height - character.height) - margin
	space_needed = max(space_needed, 0)

	var direction: Vector3 = character.up_direction if character.is_on_floor() else -character.up_direction

	var transform: Transform3D = character.global_transform

	transform.origin += direction * margin

	var result: bool = !character.test_move(transform , direction * space_needed)

	can_stand = result

