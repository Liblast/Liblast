class_name DamagableCharacterComponent
extends CharacterComponent

signal damage_recieved(attacker_pid: int, core_damage_hp: int, armor_damage_hp: int)
signal armor_destroyed(attacker_pid: int)
signal core_destroyed(attacker_pid: int)

@export var max_core_hp : int = 50 ## HP = hit points
@export var max_armor_hp : int = 100 ## HP = hit points

var core_hp : int
var armor_hp : int

## This enum describes the effect that inflicted hit (damage) has had on the target (owner of this component)
## It is used as feedback for the attacker to let them know if they did hit, was the hit terminal etc.
## - NONE: the hit inflicted no damage
## - CORE_HIT: the hit inflicted damage to character's core
## - ARMOR_HIT: the hit inflicted damage to character's armor
## - CORE_DESTROYED: the hit inflicted damage to the core and exhausted it's hit points, terminating the character
## - ARMOR_DESTROYED: the hit inflicted damage to the armor and exhausted it's hit points

#enum HitResult {NONE, DAMAGE_INFLICTED, CHARACTER_DESTROYED} # CORE_HIT, ARMOR_HIT, CORE_DESTROYED, ARMOR_DESTROYED}

## Returns a list of hitboxes so that other components (Weapons) can exclude them from collision detection
func get_hitboxes() -> Array[PhysicsBody3D]:
	if not is_node_ready():
		await ready
	return [$HitBox] # TODO later we'll have a list of hitboxes, maybe they'll have their own class etc.


func hit(attacker_pid: int, damage_hp: int):
	var armor_damage_hp
	if armor_hp > 0:
		armor_damage_hp = max(0, armor_hp - damage_hp)
		armor_hp -= armor_damage_hp

		#return HitResult.DAMAGE_INFLICTED

	var core_damage_hp = max(0, core_hp - (damage_hp - armor_damage_hp))
	core_hp -= (damage_hp - armor_damage_hp)

	damage_recieved.emit(attacker_pid, core_damage_hp, armor_damage_hp)

	assert(core_damage_hp + armor_damage_hp == damage_hp, "Armor and Core damage does not add up!")

	if armor_hp == 0:
		print(character.name + "'s armor is gone!")
		armor_destroyed.emit(attacker_pid)
	#else:
		#return

	if core_hp <= 0:
		print(character.name, "'s core is gone! Character terminated!")
		core_destroyed.emit(attacker_pid)
		return
		#return HitResult.CHARACTER_DESTROYED
	#else:
		#return HitResult.DAMAGE_INFLICTED

# Called when the node enters the scene tree for the first time.
#func _ready():
	#pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	#pass
