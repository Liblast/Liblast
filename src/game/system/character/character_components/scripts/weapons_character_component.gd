extends CharacterComponent
class_name WeaponsCharacterComponent

@export var weapons: Array[Weapon]

var model_character_component : ModelCharacterComponent
var multiplayer_character_component : MultiplayerCharacterComponent
var damagable_character_component : DamagableCharacterComponent

var hit_detection_exclude: Array[PhysicsBody3D]


func _init() -> void:
	await ready

	var interface = Interface.get_interface(character) as Interface

	for implement in interface.implements:
		if implement is ModelCharacterComponent:
			model_character_component = implement
		elif implement is MultiplayerCharacterComponent:
			multiplayer_character_component = implement
		elif implement is DamagableCharacterComponent:
			damagable_character_component = implement

	assert(is_instance_valid(damagable_character_component), "Character: Weapons can't access Damagable CharComponent")

	# we need a list of hitboxes to give to weapons so we won't shoot ourselves
	hit_detection_exclude = await damagable_character_component.get_hitboxes()
	assert(hit_detection_exclude.is_empty() == false, "hit detection is empty")

	assert(not weapons.is_empty(), "WeaponsCharacterComponent found Weapon list to be empty: " + var_to_str(weapons))
	#if weapons.is_empty():
		#return

	for w in weapons:
		w.character = character
		w.hit_detection_exclude = hit_detection_exclude

	#ensure the character is ready
	if not character.is_node_ready():
		await character.ready


func _ready():
	if not multiplayer.has_multiplayer_peer():
		return

	while not is_instance_valid(multiplayer_character_component):
		await get_tree().process_frame

	if not multiplayer_character_component.is_node_ready():
		await multiplayer_character_component.ready

	# Depending on whether this component works in first person perspective
	# We need to apply a different transform to the weapons
	for i in weapons:
		if multiplayer_character_component.is_local_player:
			%ViewWeaponRoot.add_child(i)
		else:
			%WorldWeaponRoot.add_child(i)

		# reset weapon's transform so it does not interfere with the new inherited one
		i.position = Vector3.ZERO
		i.scale = Vector3.ONE
		i.rotation_degrees = Vector3.ZERO

func _unhandled_input(event: InputEvent) -> void:
	match multiplayer_character_component.multiplayer_role:
		MultiplayerCharacterComponent.MultiplayerRole.LOCAL:
			pass
		MultiplayerCharacterComponent.MultiplayerRole.REMOTE:
			pass
		MultiplayerCharacterComponent.MultiplayerRole.SERVER:
			pass

	if Input.is_action_just_pressed(&"weapon_trigger_primary"): # LOCAL action
		weapons[0].set_trigger(event.is_pressed())
		weapons[0].set_trigger.rpc_id(1, event.is_pressed())
		#get_viewport().set_input_as_handled()
	elif Input.is_action_just_released(&"weapon_trigger_primary"):
		weapons[0].set_trigger.rpc_id(1, event.is_pressed())
		weapons[0].set_trigger(event.is_pressed())
		#get_viewport().set_input_as_handled()
	else:
		#Logger.log(["unhandled input event", event])
		pass


	#Logger.log(["Local Input Map:", InputMap.get_actions()])

	#for i in InputMap.get_actions():
		#if Input.is_action_just_pressed(i):
			#Logger.log(["Action", i, "was just pressed"])
	#print(character.get_action_name(&"weapon_trigger_primary"))


