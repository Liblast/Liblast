extends CharacterComponent
class_name MovementCharacterComponent
##Documented test

#WARNING: I did everything possible to not allow bunny hop.
#If people insist we will have to remove air control on the moving direction.
#I wish they don't, because that would make moving on air stiffer.
#TODO: Refractor apply_floor_friction()
#TODO Add "can slide up" option.
#TODO Migrate this to use a CharacterControllerComponent Class
#TODO Implement min velocity to start slide from jump
#TODO Implement min velocity to  keep sliding start slide sliding_stop_speed
#TODO add speed bump when entering sliding mode from running.
#TODO Disable snapping while sliding
#TODO DONT ALLOW RUN FROM WALKING
#TODO If you are crouched and press run equivalents to pressing un crouch.
#BUG:Sometimes, while using a cylinder shape, after crouching next to a wall
#and moving parallel to it friction isn't applied. Probably a collision detection issue.
#I'm unsure if we can do something about it. -YoSoyFreeman

@export var gravity: float = 19.6
@export var jump_impulse: float = 7.5
@export var jump_against_floor: bool = true

@export_group("States")
@export var default_state: StringName = "falling"

@export_subgroup("Standing")
@export var standing_friction: float = 20
@export var standing_friction_exp: float = 0.8
@export var standing_min_friction: float = 25

@export_subgroup("Walking")
@export var walking_acceleration: float = 200
@export var walking_crouch_acceleration: float = 100
@export var walking_friction: float = 20
@export var walking_friction_exp: float = 0.8
@export var walking_min_friction: float = 25

@export_subgroup("Running")
@export var running_acceleration: float = 300
@export var running_friction: float = 20
@export var running_friction_exp: float = 0.8
@export var running_min_friction: float = 25

@export_subgroup("Sliding")
@export_range(0, 100, 0.01, "or_greater", "hide_slider") var sliding_min_speed: float = 5
@export_range(0, 180, 0.01) var sliding_min_angle: float = 10

@export var sliding_acceleration: float = 300
@export var sliding_max_acceleration: float = 25
@export var sliding_impulse: float = 10
@export var sliding_impulse_cooldown: float = 2

@export var sliding_floor_friction: float = 20
@export var sliding_floor_friction_exp: float = 0.8
@export var sliding_floor_min_friction: float = 25

@export var sliding_friction: float = 0
@export var sliding_friction_exp: float = 0
@export var sliding_min_friction: float = 0


@export_subgroup("Falling")
@export var falling_acceleration: float = 15
@export var falling_friction: float = 10
@export var falling_friction_exp: float = 0.1
@export var falling_min_friction: float = 5

@export_group("Settings")
@export var constant_floor_friction: bool = true
@export var constant_floor_acceleration: bool = true


var state: StringName:
	set = set_state

var crouch_character_component: CrouchCharacterComponent
var multiplayer_character_component: MultiplayerCharacterComponent
var last_velocity: Vector3
var was_crouched: bool
var slide_impulse_ready: bool = true


func set_state(new_state: StringName):
	if state == new_state:
		return

	var last_state: StringName = state

	exit_state(new_state)
	state = new_state
	enter_state(last_state)


func _init():
	await ready

	state = default_state

	var interface = Interface.get_interface(character) as Interface

	for implement in interface.implements:
		if implement is CrouchCharacterComponent:
			crouch_character_component = implement
		elif implement is MultiplayerCharacterComponent:
			multiplayer_character_component = implement


func _process(_delta):
	state = get_new_state()
	last_velocity = character.velocity
	was_crouched = is_crouched()


func _physics_process(delta)-> void:
	match state:
		"standing":
			standing()

		"walking":
			walking()

		"running":
			running()

		"sliding":
			sliding(delta)

		"falling":
			falling()

		_:
			assert(false, "The state ''" + state + "'' does not exist.")

	# LOCAL character must report position and aim to server
	if multiplayer_character_component:
		if multiplayer_character_component.multiplayer_role ==\
				multiplayer_character_component.MultiplayerRole.LOCAL:

			character.set_position_absolute.rpc_id(1, character.global_position)
			character.set_aim_absolute.rpc_id(1, character.global_rotation.y, character.head.rotation.x)

		elif multiplayer_character_component.multiplayer_role ==\
				multiplayer_character_component.MultiplayerRole.SERVER:

			# TODO add prediction discrepancy analysis and server reconciliation here
			character.set_position_absolute.rpc_id(-multiplayer_character_component.player_id,\
					character.global_position) # forward position to peers
			character.set_aim_absolute.rpc_id(-multiplayer_character_component.player_id,\
					character.global_rotation.y, character.head.rotation.x) # forward aim to peers


func get_new_state() -> StringName:
	match state:
		"falling":
			if !character.is_on_floor():
				return state

			if is_crouched():
				var coplanar_speed: float = (character.velocity - character.velocity.project(character.up_direction)).length()

				if  coplanar_speed >= sliding_min_speed:
					return "sliding"

			if get_wish_direction():
				if Input.is_action_pressed(character.get_action_name(&"run")):
					if !is_crouched():
						return "running"

				return "walking"

			return "standing"

		"standing":
			if !character.is_on_floor():
				return "falling"

			if !get_wish_direction():
				return state

			if Input.is_action_pressed(character.get_action_name(&"run")) and !is_crouched():
				return "running"

			return "walking"

		"walking":
			if !character.is_on_floor():
				return "falling"

			if !get_wish_direction():
				return "standing"

			if is_crouched():
				return state

			if Input.is_action_pressed(character.get_action_name(&"run")):
				return "running"

		"running":
			if !character.is_on_floor():
				return "falling"

			if !get_wish_direction():
				return "standing"

			if !Input.is_action_pressed(character.get_action_name(&"run")):
				return "walking"

			if is_crouched():
				var coplanar_speed: float = (character.velocity - character.velocity.project(character.up_direction)).length()

				if  coplanar_speed >= sliding_min_speed:
					return "sliding"

				return "walking"

		"sliding":
			if !character.is_on_floor():
				return "falling"

			if !is_crouched():
				if !get_wish_direction():
					return "standing"

				if Input.is_action_pressed(character.get_action_name(&"run")):
					return "running"


			if (character.velocity - character.velocity.project(character.up_direction)).length() <=  sliding_min_speed:
				if !get_wish_direction():
					return "standing"

				if !Input.is_action_pressed(character.get_action_name(&"run")):
					return "running"

				return "walking"

	return state


func enter_state(last_state: StringName) -> void:
	match state:
		"sliding":
			if !slide_impulse_ready:
				return

			if !last_state == "running":
				return

			var floor_plane: Plane = Plane(character.get_floor_normal())
			var coplanar_velocity: Vector3 = character.velocity - character.velocity.project(character.up_direction)
			var coplanar_direction: Vector3 = coplanar_velocity.normalized()
			var impulse: Vector3  = coplanar_direction * sliding_impulse

			impulse = floor_plane.project(impulse)
			character.velocity += impulse
			slide_impulse_ready = false

			await get_tree().create_timer(sliding_impulse_cooldown, false, false, false).timeout

			slide_impulse_ready = true
		_:
			return


func exit_state(_new_state: StringName)-> void:
	match state:
		"falling":
			if !character.is_on_floor():
				return

			if !crouch_character_component:
				return

			crouch_character_component.speed += last_velocity.dot(character.up_direction)
		_:
			return


func get_wish_direction()-> Vector3:
	var input_vector: Vector2 = Input.get_vector(
		character.get_action_name(&"move_left"),
		character.get_action_name(&"move_right"),
		character.get_action_name(&"move_backward"),
		character.get_action_name(&"move_forward"))

	var wish_direction: Vector3 = Vector3.ZERO

	wish_direction += character.basis.x * input_vector.x
	wish_direction += -character.basis.z * input_vector.y

	return wish_direction


# used for driving character animation
func get_local_wish_direction()-> Vector2:
	return Input.get_vector(
		character.get_action_name(&"move_left"),
		character.get_action_name(&"move_right"),
		character.get_action_name(&"move_backward"),
		character.get_action_name(&"move_forward")
		)


func is_crouched() -> bool:
	if crouch_character_component:
		match crouch_character_component.state:
			"crouching":
				return true

			"crouched":
				return true

	return false


func add_gravity()-> void:
	character.apply_acceleration( -character.up_direction * gravity)


func jump()-> void:
	var floor_normal: Vector3 = character.get_floor_normal()
	var up_direction: Vector3 = character.up_direction
	var jump_direction: Vector3 = floor_normal if jump_against_floor else  up_direction

	character.floor_snap_length = 0

	character.apply_impulse(jump_direction * jump_impulse)


func standing()-> void:
	apply_floor_friction(standing_friction, standing_min_friction, standing_friction_exp)

	if Input.is_action_just_pressed(character.get_action_name(&"jump")):
		jump()


func walking()-> void:
	var acceleration: float = walking_acceleration if !is_crouched() else walking_crouch_acceleration

	apply_floor_acceleration(get_wish_direction() * acceleration)
	apply_floor_friction(walking_friction, walking_min_friction, walking_friction_exp)

	if Input.is_action_just_pressed(character.get_action_name(&"jump")):
		jump()


func running()-> void:
	var acceleration: float = running_acceleration

	apply_floor_acceleration(get_wish_direction() * acceleration)
	apply_floor_friction(running_friction, running_min_friction, running_friction_exp)

	if Input.is_action_just_pressed(character.get_action_name(&"jump")):
		jump()


#NOTE: Separates the velocity in which the character wants to slide from the rest of the velocity and applies different frictions to them.
func sliding(delta: float)-> void:
	var floor_normal: Vector3 = character.get_floor_normal()
	var floor_plane: Plane = Plane(floor_normal)
	var up_direction: Vector3 = character.up_direction
	var up_direction_plane: Plane = Plane(up_direction)

	#The direction on which a downwards velocity move us on the plane defined by the character up direction.
	var slide_wish_direction: Vector3 = Vector3.ZERO
	slide_wish_direction = floor_plane.project(-up_direction).normalized()
	slide_wish_direction = up_direction_plane.project(slide_wish_direction).normalized()

	#If the angle is less than sliding_min_angle don't slide down.
	if floor_normal.angle_to(up_direction) < deg_to_rad(sliding_min_angle):
		slide_wish_direction = Vector3.ZERO

	#Only allow user acceleration if the character is sliding.
	if !up_direction_plane.is_point_over(character.velocity):
		var acceleration_wish_direction: Vector3 = get_wish_direction()

		#Exclude slide_wish_direction from the user wish direction.
		if slide_wish_direction:
			acceleration_wish_direction = acceleration_wish_direction - acceleration_wish_direction.project(slide_wish_direction)

		#NOTICE: Sliding control is proportional to the speed on the down direction.
		var sliding_control: Vector3 = acceleration_wish_direction * sliding_acceleration * clamp(character.velocity.dot(-up_direction), 0, sliding_max_acceleration)

		apply_floor_acceleration(sliding_control)

	#Apply the force of gravity.
	add_gravity()

	#How much positive speed there is on slide_wish_direction.
	var slide_speed: float = character.velocity.dot(slide_wish_direction)
	slide_speed = max(slide_speed, 0)

	#The part of the character velocity along the slide_wish_direction.
	var slide_velocity: Vector3 = slide_wish_direction * slide_speed

	#The velocity of the character that lies on the plane defined by the character up vector.
	var coplanar_velocity: Vector3 = character.velocity - character.velocity.project(up_direction)

	#The part of the velocity to which apply standard floor friction.
	var floor_velocity: Vector3 = coplanar_velocity - slide_velocity

	#The friction to apply on the floor velocity.
	var floor_velocity_friction: float = sliding_floor_friction * pow(floor_velocity.length(), sliding_floor_friction_exp)
	floor_velocity_friction = max(sliding_floor_min_friction, floor_velocity_friction)

	#The friction to apply on the part of the velocity along the slide_wish_direction
	var slide_velocity_friction: float = sliding_friction * pow(slide_velocity.length(), sliding_friction_exp)
	slide_velocity_friction = max(sliding_min_friction, slide_velocity_friction)

	#Apply frictions.
	var new_floor_velocity: Vector3 = floor_velocity.move_toward(Vector3.ZERO, floor_velocity_friction * delta)
	var new_slide_velocity: Vector3 = slide_velocity.move_toward(Vector3.ZERO, slide_velocity_friction * delta)

	#New velocity coplanar to the character up direction created by recomposing the sliding and floor velocities.
	var new_coplanar_velocity: Vector3 = new_floor_velocity + new_slide_velocity

	#The part of the velocity who lies on the Character up direction vector.
	var up_velocity: Vector3 = character.velocity.project(up_direction)

	#Final velocity created with new_coplanar_velocity and the current velocity on the character up direction.
	var new_velocity: Vector3 = new_coplanar_velocity + up_velocity

	#Apply the new velocity.
	character.velocity = new_velocity

	if Input.is_action_just_pressed(character.get_action_name(&"jump")):
		jump()


func falling()-> void:
	add_gravity()
	character.apply_acceleration(get_wish_direction() * falling_acceleration)
	character.apply_friction(falling_friction, falling_min_friction, falling_friction_exp, true)

#INFO
##Applies an acceleration to the [Character].[br][br]
##If [param wish_acceleration] doesn't lie on the floor plane, it will be modified so the [Character] remains on the floor.[br][br]
##[param NOTE:] If [member constant_floor_acceleation] is [param true], [param wish_acceleration] will be modified to keep it's effect constant along the plane define by [member CharacterBody3D.up_direction].
func apply_floor_acceleration(wish_acceleration: Vector3)-> void:
	if !character.is_on_floor():
		return

	var floor_plane: Plane = Plane(character.get_floor_normal())

	#Apply wish_acceleration and return if wish_acceleration lies on the floor plane already.
	if floor_plane.has_point(wish_acceleration):
		character.apply_acceleration(wish_acceleration)
		return

	#If the wish acceleration lies over the floor plane.
	var lies_over_plane: bool = floor_plane.is_point_over(wish_acceleration)

	#The direction in which to look for the floor plane, using the velocity vector as a point.
	var ray_direction: Vector3 = -character.up_direction if lies_over_plane else character.up_direction

	var corrected_acceleration: Vector3

	#Correct the acceleration so it's effect along the plane defined by the character up direction remains constant.
	if constant_floor_acceleration:
		corrected_acceleration = floor_plane.intersects_ray(wish_acceleration, ray_direction)

		if corrected_acceleration:
			character.apply_acceleration(corrected_acceleration)

		return

	#Project the acceleration into the floor plane if going upwards.
	if ray_direction == character.up_direction:
		corrected_acceleration = floor_plane.project(wish_acceleration)
		character.apply_acceleration(corrected_acceleration)

		return

	#Modify the acceleration to keep the character on the floor plane if going downwards.
	corrected_acceleration = floor_plane.intersects_ray(wish_acceleration, ray_direction)

	if corrected_acceleration:
		character.apply_acceleration(corrected_acceleration)

#INFO
#TODO: Refractor this class using the "Is above plave" method for the direction check.
##Applies friction to the [Character].[br][br]
##If [member constant_floor_friction] is [param true], the speed along [CharacterBody3D.up_direction]
##will be ignored while calculating friction and the [member CharacterBody3D.velocity] modified in order
##to keep the [Character] on the floor.
func apply_floor_friction(friction: float, min_friction: float = 0, exponent: float = 1):
	if !character.is_on_floor():
		return

	var previous_velocity: Vector3 = character.velocity

	character.apply_friction(friction, min_friction, exponent, constant_floor_friction)

	if !constant_floor_friction:
		return

	var velocity_difference: Vector3 = character.velocity - previous_velocity

	var floor_plane: Plane = Plane(character.get_floor_normal())


	if floor_plane.has_point(velocity_difference):
		return

	#If the velocity difference lies over the floor plane.
	var lies_over_plane: bool = floor_plane.is_point_over(velocity_difference)
	#The direction in which to look for the floor plane, using the velocity difference as a point.
	var ray_direction: Vector3 = -character.up_direction if lies_over_plane else character.up_direction


	var corrected_velocity_difference: Vector3 = floor_plane.intersects_ray(velocity_difference, ray_direction)


	if !corrected_velocity_difference:
		#CAUTION: We should always find a plane in this case, but numerical error may make it fail.
		#In case it did we can't do nothing, but we can not allow the code to continue
		#Or it would try to assing a null value to a vector3, making the game crash.
		return

	#The correction we need to apply in order to prevent the character from
	#leaving the plane as a result of not appliying vertical friction.
	var up_velocity_correction: Vector3

	#Only care about the up direction.
	up_velocity_correction = corrected_velocity_difference.project(character.up_direction)

	#Is a friction, limits it's effects to the up speed.
	up_velocity_correction = up_velocity_correction.limit_length(abs(character.velocity.dot(character.up_direction)))

	character.velocity += up_velocity_correction

