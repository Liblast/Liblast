@tool
extends CharacterComponent
class_name ModelCharacterComponent

## This CharacterComponent manages the character's 3D model nad things related to it like skeletal animation
## It exposes bone attachemenets that other CharacterComponents cana use to for example position weapon models, cameras etc.

var movement_character_component: MovementCharacterComponent# = $"../MovementCharacterComponent"
@export var body_mesh: MeshInstance3D
@export var animation_player: AnimationPlayer
@export var animation_tree: AnimationTree

@export var bone_attachements : Array[BoneAttachment3D]

var multiplayer_character_component : MultiplayerCharacterComponent


var locomotion_wish_direction : Vector2
@onready var jump_playback : AnimationNodeStateMachinePlayback = animation_tree.get("parameters/JumpStateMachine/playback")


@export var accent_color: Color:
	set(value):
		accent_color = value
		set_accent_color(accent_color)

#@rpc("any_peer", "call_local", "reliable")
func set_accent_color(color: Color) -> void:
	if not body_mesh:
		return
	var material = body_mesh.get_surface_override_material(3)
	material.albedo_color = color
	body_mesh.set_surface_override_material(3, material)


@rpc("any_peer", "unreliable_ordered", "call_local")
func aim_seek_request(value) -> void:
	#print("aim_see_request ", value)
	animation_tree.set(&"parameters/AimSeek/seek_request", value)
	#print("animation_tree aim set to: ", animation_tree.get(&"parameters/AimSeek/seek_request"))

	#if multiplayer.get_unique_id() == 1 and multiplayer.get_remote_sender_id() != 1:
		#aim_seek_request.rpc_id(- int(str(character.name)), value)


#@rpc("any_peer", "unreliable_ordered", "call_local")
#func locomotion_travel_to(state:StringName) -> void:
	#locomotion.travel(state)
	#if multiplayer.get_unique_id() == 1:
		#locomotion_travel_to.rpc_id(- int(str(character.name)), state)


func _init() -> void:
	await ready

	var interface = Interface.get_interface(character) as Interface

	for implement in interface.implements:
		if implement is MovementCharacterComponent:
			movement_character_component = implement
		elif implement is MultiplayerCharacterComponent:
			multiplayer_character_component = implement


func setup_camera_bone_atachement():
	if bone_attachements.is_empty(): return
	for i in bone_attachements:
		if i.name == "Head":
			character.head.model_head_bone_attachement = i
			return


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	super()

	animation_player.current_animation = &"Stand"
	aim_seek_request(1) # reset aim
	animation_tree.set(&"parameters/LocomotionBlendSpace/blend_position", Vector2.ZERO) # set character standing still

	setup_camera_bone_atachement()



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if not character:
		return

	if character.camera.current:
		animation_tree.set(&"parameters/NoHeadBlend/blend_amount", 1.0)
		#Logger.log("No HEAD")
	else:
		animation_tree.set(&"parameters/NoHeadBlend/blend_amount", 0.0)
		#Logger.log("HEAD")

	#animation_tree.set(&"parameters/AimAngle/seek_request", 1)5
	aim_seek_request(abs((- character.head.rotation_degrees.x / 90.0) + 1))

	locomotion_wish_direction = locomotion_wish_direction.slerp(movement_character_component.get_local_wish_direction(), delta * 10)

	animation_tree.set(&"parameters/LocomotionBlendSpace/blend_position", Vector2(locomotion_wish_direction.x, locomotion_wish_direction.y))

	match movement_character_component.state:
		"walking":
			animation_tree.set(&"parameters/LocomotionTimeScale/scale", 1)
		"running":
			animation_tree.set(&"parameters/LocomotionTimeScale/scale", 1.5)


	if movement_character_component.state == "falling":
		animation_tree.set("parameters/JumpStateMachine/conditions/land", false)
		animation_tree.set("parameters/JumpOneShot/request", 1) # start
		jump_playback.travel(&"Jump")
		#jump_playback.travel_to(&"MidAir")
	else:
		animation_tree.set("parameters/JumpStateMachine/conditions/land", true)
		animation_tree.set("parameters/JumpOneShot/request", 3) # fade-out
		#jump_playback.stop()
	#if Input.is_action_just_pressed(character.get_action_name(&"jump")):
		#locomotion_travel_to(&"Jump")
#
