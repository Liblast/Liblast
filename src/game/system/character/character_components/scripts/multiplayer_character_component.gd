extends CharacterComponent
class_name MultiplayerCharacterComponent
## Adds networking features to a CharacterController, allowing for online play

## The networking component will behave differently when running on a locally controller character, a remotely controlled one or on the server
enum MultiplayerRole {
	LOCAL, ## takes local inputs from the player and sends them to the server
	REMOTE, ## applies state of a remote character as provided by the server
	SERVER, ## takes in player's local control and state, processes it and distributes to otehr peers
	}

signal multiplayer_role_changed(role: MultiplayerRole)

var multiplayer_role : MultiplayerRole:
	set(value):
		multiplayer_role = value
		multiplayer_role_changed.emit(value)
		is_local_player = value == MultiplayerRole.LOCAL

		request_ready()

var player_id : int :
	get: return int(str(character.name))

var is_local_player : bool = false

## Actions that are tracked and replicated by the multiplayer component
const ACTIONS : Array[StringName] = [
	&"jump",
	&"move_left",
	&"move_right",
	&"move_forward",
	&"move_backward",
	&"crouch",
	&"hold_crouch",
	&"run",
	&"look_up",
	&"look_down",
	&"look_left",
	&"look_right",
	&"weapon_trigger_primary",
	#&"weapon_trigger_secondary",
	#&"weapon_reload",
	#&"weapon_select_previous"
	#&"weapon_select_primary",
	#&"weapon_select_secondary",
	#&"weapon_select_tertiary",

	#&"change_camera",
]


func _ready():
	assert(get_parent() is Character, "MutliplayerCharacterComponent must be a direct child of the character!")
	character = get_parent()

	# Add actions for this player.
	for action in ACTIONS:
		var action_name = action + "_PID_%d" % player_id
		if not InputMap.has_action(action_name):
			InputMap.add_action(action_name)
	if player_id == multiplayer.get_unique_id():
		# Activate the camera for the local player
		character.camera.make_current()
	else:
		# Only process local input for the local player
		set_process(player_id == multiplayer.get_unique_id())


func _exit_tree():
	# Remove actions for this player
	for action in ACTIONS:
		var action_name = action + "_PID_%d" % player_id
		if InputMap.has_action(action_name):
			InputMap.erase_action(StringName(action + "_PID_%d" % player_id))


## Poll and trigger inputs for multiplayer character
func _process(_delta):

	# decide who gets the rpc call
	var rpc_target_id : int
	if multiplayer_role == MultiplayerRole.LOCAL:
		rpc_target_id = 1
	elif multiplayer_role == MultiplayerRole.SERVER:
		rpc_target_id = -player_id
	else:
		rpc_target_id = 0 # this will make the rpc call get skipped

	for i in range(ACTIONS.size()):
		var action := ACTIONS[i]
		if Input.is_action_just_pressed(action):
			trigger(i, true) # LOCAL
			if rpc_target_id != 0:
				trigger.rpc_id(rpc_target_id, i, true) # to SERVER or REMOTE
		if Input.is_action_just_released(action):
			trigger(i, false) # LOCAL
			if rpc_target_id != 0:
				trigger.rpc_id(rpc_target_id, i, false) # to SERVER or REMOTE


## Trigger an action
@rpc("call_local", "any_peer", "unreliable_ordered")
func trigger(idx: int, pressed: bool, strength: float = 1.0):
	match multiplayer.get_remote_sender_id():
		0: # LOCAL call ?
			#Logger.log([character.name, "recieved RPC fom PID 0"], Logger.MessageType.QUESTION)
			if player_id == multiplayer.get_unique_id():
				pass
			else:
				push_warning("Character recieved an invalid local call")
		1: # SERVER
			if multiplayer.is_server() == false:
				pass
			else:
				push_warning("Character recieved an invalid server local call")
		_: # other
			if multiplayer.get_remote_sender_id() == player_id:
				#prints(character.name, "recieved appropriate call to trigger action", ACTIONS[idx])
				pass # LOCAL
			else:
				push_warning("Character", character.name, " recieved unsolicited call from PID ", multiplayer.get_remote_sender_id())
				return

	#print("Action passed tests")

	if idx >= ACTIONS.size() or idx < 0:
		push_error("Action is invalid")
		return
	var action := StringName(ACTIONS[idx] + "_PID_%d" % player_id)
	if Input.is_action_pressed(action) == pressed:
		#prints("Ignoring already pressed action", ACTIONS[idx])
		return

	if pressed:
		#Logger.log(["Pressing action", action], Logger.MessageType.INFO)
		Input.action_press(action, strength)
	else:
		#Logger.log(["Releasing action", action], Logger.MessageType.INFO)
		Input.action_release(action)

