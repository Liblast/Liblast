extends CharacterComponent
class_name AimLookCharacterComponent

@export_group("Mouse settings")
@export_range(0, 100) var mouse_sensitivity: float = 0.075
@export_group("Joypad settings")
@export var joypad_sensitivity:float = 2

#TODO Set default path once is merged!
@export var joypad_sensitivity_curve: Curve

var multiplayer_character_component : MultiplayerCharacterComponent

#var recieve_remote_input : bool = false
var transmit_remote_input : bool = false


func _init():
	await ready

	joypad_sensitivity_curve.bake()

	var interface = Interface.get_interface(character) as Interface

	for implement in interface.implements:
		if implement is MultiplayerCharacterComponent:
			multiplayer_character_component = implement

	# Check what multiplayer role do we have and set parameters accordingly
	if multiplayer_character_component:
		match multiplayer_character_component.multiplayer_role:
			MultiplayerCharacterComponent.MultiplayerRole.LOCAL:
				#recieve_remote_input = false
				transmit_remote_input = true
			MultiplayerCharacterComponent.MultiplayerRole.REMOTE:
				#recieve_remote_input = true
				transmit_remote_input = false
			MultiplayerCharacterComponent.MultiplayerRole.SERVER:
				#recieve_remote_input = true
				transmit_remote_input = false


func _ready():
	character = get_parent() # assuming we're a direct child of Character
	await character.ready
	var path: String = str(get_tree().current_scene.get_path()) + "/" + str(get_tree().current_scene.get_path_to(self))
	assert(character.head, "AimLookCharacterComponent requires the Character to have a CharacterHead as a child of Character. " + " (Path: " + path + ")" )


## Only called on LOCAL character
func _unhandled_input(event)-> void:
	if multiplayer_character_component.multiplayer_role !=\
		MultiplayerCharacterComponent.MultiplayerRole.LOCAL:
		return

	if Input.mouse_mode != Input.MOUSE_MODE_CAPTURED:
		if event is InputEventKey:
			if event.is_action_pressed("ui_cancel"):
				pass #get_tree().quit()

		if event is InputEventMouseButton:
			if event.button_index == 1:
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				get_viewport().set_input_as_handled()

		return

	if event is InputEventKey:
		if event.is_action_pressed("ui_cancel"):
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			get_viewport().set_input_as_handled()

		return

	if event is InputEventMouseMotion:
		mouse_aim_look(event)
		get_viewport().set_input_as_handled()


func _process(delta):
	joypad_aim_look(delta)


func mouse_aim_look(event: InputEventMouseMotion)-> void:
	if not transmit_remote_input:
		return

	var viewport_transform: Transform2D = get_tree().root.get_final_transform()
	var motion: Vector2 = event.xformed_by(viewport_transform).relative

	motion *= mouse_sensitivity

	# do it locally
	character.add_yaw(motion.x)
	character.add_pitch(-motion.y)

	# tell the server
	character.add_yaw.rpc_id(1, motion.x)
	character.add_pitch.rpc_id(1, -motion.y)

	#character.clamp_pitch() # this is done automatically in the character controller now


# WARNING this function sometimes causes ~20ms lag spikes
func joypad_aim_look(delta)-> void:
	var joypad_input:Vector2 = Input.get_vector(
	"look_left",
	"look_right",
	"look_down",
	"look_up"
	)
	var joypad_input_lenght:float = joypad_input.length()
	var sensitivity_curve_multiplier:float = joypad_sensitivity_curve.sample_baked(joypad_input_lenght)
	var motion:Vector2 = Vector2(joypad_input.x, joypad_input.y)

	motion *= joypad_sensitivity
	motion *= sensitivity_curve_multiplier
	motion *= delta

	character.add_yaw(motion.x)
	character.add_pitch(motion.y)
	character.clamp_pitch()
