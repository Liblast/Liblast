extends GeometryInstance3D

@export var fade_time: float = 1

var target_visible: bool = true

var tween : Tween


func set_visible_override(value: bool):
	#print("Set override")
	target_visible = value

	if self.visible == value:
		return

	var target_transparency: float
	match value:
		true:
			target_transparency = 0
		false:
			target_transparency = 1

	if is_instance_valid(tween):
		if tween.is_running():
			tween.kill()

	tween = create_tween()
	tween.tween_property(self, "transparency", target_transparency, fade_time)

	#if target_visible == false:
		#tween.finished.connect(func(): self.visible = false)
	#else:
		#self.visible = true


func _set(property: StringName, value) -> bool:
	if property == &"visible":
		set_visible_override(value)
		return true
	else:
		return false
		#set(property, value)


func _process(delta):
	print("Transparency ", self.transparency)
