extends CharacterComponent

@export var camera_raycast : RayCast3D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	camera_raycast.target_position = get_viewport().get_camera_3d().position
	camera_raycast.force_raycast_update()
	%NameTag.visible = not camera_raycast.is_colliding()

