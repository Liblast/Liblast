extends CharacterMetadata
class_name CharacterMatchMetadata

var health: int ## current health / hit_points
var current_class: StringName ## Currently played character class
var team: StringName ## Current team the character plays in
var frags: int ## Current match frag count
var deaths: int ## Current match death count
var shots_fired_by_weapon: Dictionary ## StringName : int
var shots_hit_by_weapon: Dictionary ## StringName : int
var damage_taken_by_weapon: Dictionary ## StringName : int
var damage_taken_by_player: Dictionary ## StringName : int
