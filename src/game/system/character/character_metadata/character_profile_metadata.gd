extends CharacterMetadata
class_name CharacterProfileMetadata

var display_name: String ## Display name of the player's character
var bio : String ## Text player wants to share about themselves
var avatar_hash : String ## Hash addresing the player's avatar image resource
var preferred_color: Color ## Preferred accent (detail) color for deathmatch
var preferred_team: StringName ## default team to be selected (when possible)
var preferred_class: StringName ## Default class to be selected
var color_by_team: Array[Color] ## Preferred body/ accent colors for each team
