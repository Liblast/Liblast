extends Resource
class_name CharacterMetadata

var player_peer_id: int
var player_account_id: String
var authenticated: bool
var session_token: String
