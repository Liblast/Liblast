extends CharacterMetadata
class_name CharacterStatsMetadata

var total_seconds_played: int ## All time spent in-game
var total_seconds_played_idle: int ## All time spent doing nothing
var total_seconds_played_active: int ## All time spent actively playing the game
var seconds_played_by_class: Dictionary ## StringName: int
var seconds_played_by_team: Dictionary ## StringName: int
var total_matches_played: int
var total_matches_won: int
var total_matches_lost: int
var total_matches_forfeited: int
var total_matches_abandoned: int
var total_deaths: int
var total_frags: int
var total_shots_fired_by_weapon: Dictionary ## StringName : int
var total_shots_hit_by_weapon: Dictionary ## StringName : int
var first_match_played: int ## UTC unix time
var last_match_played: int ## UTC unix time
var account_created: int ## UTC unix time
var first_login: int ## UTC unix time
var last_login: int ## UTC unix time
