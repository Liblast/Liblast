## Keeps complete information about the currently running game
class_name GameState extends Node

signal map_ready

const REFLECTION_PROBE_RENDERING_TIMEOUT = 3

@export var is_server : bool = false
@export var game_config : GameConfig

@export var spatial_origin_position : Vector3

var map : Node3D


func _ready() -> void:
	Logger.set_multiplayer(multiplayer) # tell the Logger singleton what network peer this game state is bound to for better log readability
	%LoadingScreen.show()

	# only the server is allowed so spawn or despawn characters
	%CharacterSpawner.set_multiplayer_authority(1)
	%CharacterSpawner.spawn_function = _spawn_function
	%Characters.set_multiplayer_authority(1)
	%Characters.position = spatial_origin_position

	if is_server: # no need to render characters if we're a server
		%Characters.hide()

	%CharacterSpawner.add_spawnable_scene("res://system/character/base_character/scenes/base_character.tscn")
	#if is_server:
		#%CharacterSpawner.add_spawnable_scene("res://system/character/character_server.tscn")
		#%CharacterSpawner.add_spawnable_scene("res://system/character/character_client.tscn")
		#%CharacterSpawner.add_spawnable_scene("res://system/character/character_real.tscn")


@rpc("authority", "reliable", "call_remote")
func load_map():
	Logger.log(["Loading map packed scene from   ", game_config.map_scene_path], Logger.MessageType.INFO)
	if is_instance_valid(map): # refuse to load a map twice
		return
	var time_msec = Time.get_ticks_msec()
	var map_scene = load(game_config.map_scene_path)
	var map_instance = map_scene.instantiate()
	assert(map_instance is Map, "Game State loaded a map that is of type " + str(map_instance))

	map_instance.name = "Map"
	add_child(map_instance)
	map_instance.global_position = spatial_origin_position
	map = map_instance

	if is_server: # no need to render the map if we're running a server
		map.hide()

	time_msec = Time.get_ticks_msec() - time_msec
	Logger.log(["Map loaded and spawned in", time_msec, "msec"], Logger.MessageType.SUCCESS)
	await get_tree().create_timer(REFLECTION_PROBE_RENDERING_TIMEOUT).timeout
	map_ready.emit()
	%LoadingScreen.hide()


@rpc("authority", "reliable", "call_local")
func spawn_character(client_id) -> void:
	if multiplayer.is_server:
		Logger.log(["Server spawning character"], Logger.MessageType.INFO)
	else:
		Logger.log(["Client spawning character - not allowed"], Logger.MessageType.ERROR)
		return

	# Ask the map for a spawn point
	var spawnpoint : Node3D = map.get_spawn_point()
	assert(is_instance_valid(spawnpoint), "Map did not provide a valid spawnpoint: " + str(spawnpoint))

	var spawn_pos = spawnpoint.get_global_position()
	var spawn_aim = Vector2(0, spawnpoint.get_global_rotation().y)

	%CharacterSpawner.spawn({
		"client_id" = client_id,
		#"client_name" = client_name,
		"position" = spawn_pos,
		"aim" = spawn_aim,
		})

	Logger.log(["Spawning character %d on peer %d." % [client_id, multiplayer.get_unique_id()]], Logger.MessageType.INFO)


@rpc("authority", "reliable", "call_local")
func destroy_character(client_id : int) -> void:
	var character = %Characters.get_node_or_null(str(client_id))
	if is_instance_valid(character):
		character.queue_free()
		Logger.log(["Destroyed character %d on peer %d." % [client_id, multiplayer.get_unique_id()]], Logger.MessageType.SUCCESS)
	else:
		var data = [client_id, str(character)]
		push_error("Attempted to destroy character %d, but the character is %s" % data)


func _spawn_function(data : Dictionary) -> Node:
	var character : Node = load(%CharacterSpawner.get_spawnable_scene(0)).instantiate()
	character.name = str(data.client_id)
	character.position = data.position
	character.rotation.y = data.aim.y

	# CLIENT
	if not is_server: # LOCAL
		if data.client_id == multiplayer.get_unique_id():
			Logger.log(["Spawned client local character"], Logger.MessageType.SUCCESS)
			#character.set_multiplayer_authority(1)
			#character.camera.set_current(true)
		else: # REMOTE
			Logger.log(["Spawned client", data.client_id, "remote character"], Logger.MessageType.SUCCESS)
			#character.set_multiplayer_authority(1)
			#character.camera.set_current(false)

	# SERVER
	else:
		Logger.log(["Spawned client", data.client_id, "authoritative character"], Logger.MessageType.SUCCESS)
		#character.set_multiplayer_authority(1)
		#character.camera.set_current(false)

	return character
