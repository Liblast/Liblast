@tool # Needed so it runs in editor.
extends EditorScenePostImport

const MATERIAL_LIB_CACHE_TTL = 3 # seconds

const MISSING_MATERIAL : String = "res://system/import/missing_material.tres"

var material_lib_path = "res://data/game/materials/"

var lib_metadata_file_name = ".material_lib"
var lib_metadata_path = material_lib_path.path_join(lib_metadata_file_name)

#var last_error : String = ""

static var material_lib : Dictionary

# This sample changes all node names.
# Called right after the scene is imported and gets the root node.

func update_material_lib() -> void:
	material_lib.clear()
	find_materials_in_path(material_lib_path)
	print("Material library updated. Found %d materials:\n%s" % [material_lib.keys().size(), var_to_str(material_lib)])

	var lib_metadata = FileAccess.open(lib_metadata_path,FileAccess.WRITE_READ)
	lib_metadata.store_var({"last_updated_at" : Time.get_unix_time_from_system()})
	lib_metadata.close()


func _post_import(scene):
	if material_lib.is_empty():
		#print("Analyzing material library...")
		update_material_lib()
	else:
		#print("Material library exists. Found %d materials:\n%s" % [material_lib.keys().size(), var_to_str(material_lib)])

		if FileAccess.file_exists(lib_metadata_path):
			var lib_metadata = FileAccess.open(lib_metadata_path,FileAccess.READ)
			if lib_metadata.get_error() == OK:
				#print("Loading metadata...")
				var metadata = lib_metadata.get_var() as Dictionary
				#print("Got metadata: %s" % [str(metadata)])
				if metadata.last_updated_at > Time.get_unix_time_from_system() - MATERIAL_LIB_CACHE_TTL:
					#print("Material library is no older than %d seconds, reusing" % [MATERIAL_LIB_CACHE_TTL])
					lib_metadata.close()
				else:
					#print("Material library is older than %d seconds, re-analyzing" % [MATERIAL_LIB_CACHE_TTL])
					lib_metadata.close()
					update_material_lib()
			else:
				#print("Can't determine how long it's been since the material library was updated, re-analyzing")
				lib_metadata.close()
				update_material_lib()
		else:
			#print("No library metadata was found, re-analyzing")
			update_material_lib()

	# Change all node names to "modified_[oldnodename]"
	#iterate(scene)

	iterate(scene)

	return scene # Remember to return the imported scene


func find_materials_in_path(path: String):
	var dir = DirAccess.open(path)
	var files = dir.get_files()

	for i in (files as Array[String]):
		#print(i)
		var file_path = path.path_join(i)
		var mat_name = i.trim_suffix(".tres")
		#print("Found material %s from file %s" % [mat_name, file_path])
		material_lib[mat_name] = file_path

	var subdirs = dir.get_directories()
	for i in subdirs:
		find_materials_in_path(path.path_join(i))


func iterate(node):
	if not is_instance_valid(node):
		push_error("Node is invalid: ", node)
		#var rich_error = "[color=ff0000][b]ERROR[/b][/color] [color=ff00ff]while importing MeshInstance [b]%s[/b] in scene %s: [/color][color=00ff00]\"[/color][b][color=ffffff]%s[/color][/b][color=00ff00]\" node is invalid!" % [node.name.strip_edges(), get_source_file(), res_name]
		#print_rich(rich_error)
		return
	var mesh_instance = node as MeshInstance3D
	if mesh_instance:
		print("Procesing %s" %[str(mesh_instance.name)])
		var surface_count = mesh_instance.mesh.get_surface_count()
		#print("Mesh has %d surfaces" % [surface_count])
		for i in range(surface_count):
			var mat : Material = mesh_instance.mesh.surface_get_material(i)
			var res_name = mat.resource_name
			#print("Searching for %s in material library..." % [res_name])

			if material_lib.keys().has(res_name):
				#print("Setting new material resource path...")
				mat.take_over_path(material_lib[res_name])
				mesh_instance.mesh.surface_set_material(i, mat)
				#print("Processed %s on %s" % [res_name, mesh_instance.name]);
			else:
				var rich_error = "[color=ff0000][b]ERROR[/b][/color] [color=ff00ff]while importing MeshInstance [b]%s[/b] in scene %s: [/color][color=00ff00]\"[/color][b][color=ffffff]%s[/color][/b][color=00ff00]\" material was [b]not found in the library!" % [node.name.strip_edges(), get_source_file(), res_name]
				print_rich(rich_error)
				var error = "Error while importing MeshInstance %s in scene %s - \"%s\" material was not found in the library!" % [node.name.strip_edges(), get_source_file(), res_name]
				push_error(error)

				mat.take_over_path(MISSING_MATERIAL)
				mesh_instance.mesh.surface_set_material(i, mat)

	else:
		print("Node %s is not a MeshInstance" % [node.name])
		pass

	for child in node.get_children():
		iterate(child)
