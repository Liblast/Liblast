extends Control
@export_category("Benchmark Settings")
@export var duration_seconds: int = 15
#@export var fixed_resolution: bool
@export var camera_speed_curve: Curve
var first_frame_index: int
var first_tick_msec: int

@export_category("Manual Control")
@export var manual_control : bool = false
@export var manual_stop : bool = true
@export var manual_reverse : bool = false
@export_range(0,1000) var manual_speed : int = 15
@export_category("Progress")

@export var camera_path_progress: float = 0.0:
	set(value):
		camera_path_progress = value
		var corrected_value = camera_speed_curve.sample_baked(value)
		if get_node_or_null("%CameraPathFollow3D"):
			%CameraPathFollow3D.progress_ratio = corrected_value
		if get_node_or_null("%TargetPathFollow3D"):
			%TargetPathFollow3D.progress_ratio = corrected_value

var perf: Array[datapoint] # performance history

class datapoint:
	var time_elapsed_ms: int
	var frame_index: int
	var frame_time: int
	var frame_delta_time_ms: int

	func from_dict(dict: Dictionary):
		time_elapsed_ms = dict.t_ed as int
		frame_index = dict.f_idx as int
		frame_time = dict.f_t as int
		frame_delta_time_ms = dict.fdms as int

	func to_dict() -> Dictionary:
		return {
			"t_ed": time_elapsed_ms,
			"f_idx": frame_index,
			"f_t": frame_time,
			"fdms": frame_delta_time_ms,
			}


func on_benchmark_finished():
	set_process(false)
	DirAccess.make_dir_recursive_absolute("user://benchmarks")
	var file_name = "user://benchmarks/".path_join(str(name) + "_" + Time.get_datetime_string_from_system() + ".log")
	var file = FileAccess.open(file_name, FileAccess.WRITE)

	for dp in perf:
		file.store_string(var_to_str(dp.to_dict()))
	#file.store_string(var_to_str(perf))
	file.close()

	var frame_time_min: int = 1000000
	var frame_time_max: int = 0
	var frame_time_avg: int = perf[0].time_elapsed_ms
	#var frame_time_min_1percent: int = perf[0].time_elapsed_ms
	#var frame_time_min_10percent: int = perf[0].time_elapsed_ms
	var frames_total: int = 0
	var time_total: int = 0
	var time_elapsed_since_last_frame: int = perf[0].time_elapsed_ms
	var frame_time_distribution: Dictionary = {}

	#for i in range(0,1000):
		#frame_time_distribution[i] = 0

	var frame_time: int
	for dp in perf:
		frame_time = dp.frame_delta_time_ms #time_elapsed_since_last_frame - dp.time_elapsed_ms
		frame_time_min = min(frame_time_min, frame_time)
		frame_time_max = max(frame_time_max, frame_time)
		frame_time = clampi(frame_time, 0, 999)

		if frame_time_distribution.keys().has(frame_time):
			frame_time_distribution[frame_time] += 1
		else:
			frame_time_distribution[frame_time] = 1

		time_total += frame_time
		frames_total += 1
		#print(time_elapsed_since_last_frame, " ", frame_time)
		time_elapsed_since_last_frame += dp.time_elapsed_ms

	var frame_time_array: Array = []
	for i in frame_time_distribution.keys():
		frame_time_array.append({
			"ms": i, # frame_time
			"x": frame_time_distribution[i] # occurances
			})
	frame_time_array.sort_custom(func(a,b): return a.x > b.x)

	print(var_to_str(frame_time_distribution))

	print("benchmark finished. Captured data was saved in ", file_name)

	get_tree().quit()

	#var line:Line2D = $"FPS graph/Line2D"
	#line.clear_points()
#
	#for i in frame_time_array:
		#line.add_point(Vector2(
			#-(i.x / frame_time_array.size()) * 400.0,
			#i.ms * 25 + 200,
		#))

func start_benchmark():
	camera_speed_curve.bake()
	Engine.max_fps = 0
	DisplayServer.window_set_vsync_mode(DisplayServer.VSYNC_DISABLED)
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	await get_tree().create_timer(1).timeout
	var tween = create_tween()
	tween.tween_property(self, "camera_path_progress", 1.0, duration_seconds).from(0)
	#tween.tween_property($CameraPath3D/PathFollow3D, "progress_ratio", 1.0, duration_seconds).from(0)
	#tween.parallel()
	#tween.tween_property(%PathFollow3D, "progress_ratio", 1.0, duration_seconds).from(0)
	tween.finished.connect(on_benchmark_finished)
	first_frame_index = Engine.get_frames_drawn()
	first_tick_msec = Time.get_ticks_msec()
	tween.play()
	set_process(true)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_process(false)
	if not manual_control:
		start_benchmark()
	else:
		set_process(true)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if not manual_control:
		var dp = datapoint.new()
		dp.frame_index = Engine.get_frames_drawn() - first_frame_index
		dp.time_elapsed_ms = Time.get_ticks_msec() - first_tick_msec
		dp.frame_delta_time_ms = round(delta * 1000.0)
		dp.frame_time = round(Performance.get_monitor(Performance.TIME_PROCESS) * 1000.0)
		#print(Performance.get_monitor(Performance.TIME_PROCESS) * 1000.0) # Prints the FPS to the console.
		print(var_to_str(dp.to_dict()))

		perf.append(dp)
	elif not manual_stop:
		#await ready
		camera_path_progress += delta * (manual_speed as float / 1000.0) * -1.0 if manual_reverse else 1.0
