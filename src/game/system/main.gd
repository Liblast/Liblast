extends Node


func _on_host_button_pressed() -> void:
	$Menu.hide()
	$ServerViewport/Server.start_server()
