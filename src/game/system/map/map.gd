class_name Map extends Node3D

@export var map_name := "map"


func get_spawn_point() -> Node3D:
	get_scene_instance_load_placeholder()
	return get_tree().get_nodes_in_group(&"character_spawners").pick_random()
