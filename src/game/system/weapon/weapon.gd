extends Node3D
class_name Weapon

const MAX_RAYCAST_ITERATIONS = 4

var character : Character

var hit_detection_exclude: Array[PhysicsBody3D]

#@export_category("Base stats")
@export var full_auto: bool = true
@export var base_damage: int = 15
@export var rounds_per_minute: int = 400
@export var magazine_size: int = 30
@export var reload_time_seconds: float = 2.0
@export var bullet_random_spread_degrees: float = 0.1
#@export_category("Recoil")
@export var recoil_amount: float = 1
@export var recoil_pattern = Polygon2D

@export_range(1,MAX_RAYCAST_ITERATIONS) var max_penetration_steps: int = 3


var trigger: bool = false

var shooting_tween: Tween
var reload_tween: Tween
var remaining_shots: int

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	remaining_shots = magazine_size

	if not is_instance_valid(character):
		Logger.log("Weapon has no associated character. This might be a problem unless used in a test", Logger.MessageType.WARNING)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
	#pass

func shoot():
	if can_shoot():
		trigger = true
		$Muzzle/MuzzleFlash.flash()
		%ShellEmitter.emitting = true
		var tween = create_tween()
		tween.tween_property(%ShellEmitter, "emitting", false, 0).set_delay(0.01)
		tween.play()

	else:
		reload()
		return

	remaining_shots -= 1
	var hit = check_hitscan_hit()
	#Logger.log(["Shooting: ", hit])
	if hit.is_empty():
		#Logger.log("Ray returned an empty dict. This could be a problem.", Logger.MessageType.WARNING)
		pass
	else:
		for i in hit.hits:
			if i is Damagable:
				i.damage(base_damage)
	#return true


@rpc("call_local","any_peer","reliable")
func set_trigger(pressed:bool):
	if multiplayer.has_multiplayer_peer():

		var rpc_pid = multiplayer.get_remote_sender_id()
		match rpc_pid:
			0:	Logger.log(["set_trigger: local call"])
			1:	Logger.log(["set_trigger: server's remote call"])
			_:	Logger.log(["set_trigger: illegal remote call from", rpc_pid])
#
		#Logger.log(["Are we authority?", is_multiplayer_authority()])

	if pressed:
		if full_auto:
			if is_instance_valid(shooting_tween):
				if shooting_tween.is_running():
					shooting_tween.kill()
			shooting_tween = create_tween().set_loops()
			shooting_tween.tween_callback(shoot).set_delay(60.0 / rounds_per_minute)
			shooting_tween.play()

			shoot() # trigger first shot immediatwely, because the tween will start with a delay
		else:
			shoot() # single shot
	else:
		trigger = false
		if is_instance_valid(shooting_tween):
			if shooting_tween.is_running():
				shooting_tween.kill()


@export_flags_3d_physics var collision_mask : int = 0xFFFFFFFF

# Returns list of players shot
func cast_ray(from : Vector3, to : Vector3):
	var hits = []
	var ray_targets = []

	if hit_detection_exclude.is_empty():
		Logger.log(["Hit detection exclude array is empty, but the gun is shooting. This could be a problem."], Logger.MessageType.WARNING)

	var exclude = [character] if character else [self]
	exclude.append_array(hit_detection_exclude)

	var space_state = get_world_3d().direct_space_state

	var iteration: int = 0
	while iteration < max_penetration_steps:
		iteration += 1
		var ray_params = PhysicsRayQueryParameters3D.\
		create(from, to, collision_mask, exclude)

		var ray = space_state.intersect_ray(ray_params)
		ray_targets.append(ray_params.to)

		#Logger.log(ray)

		if ray == {}:
			#Logger.log("ray is empty")
			return  { "hits" : hits, "targets": ray_targets }


		# Spawn impact effect
		var impact_scene = preload("res://data/game/vfx/bullet_impact.tscn").instantiate()

		get_tree().root.add_child(impact_scene)
		impact_scene.global_position = ray.position
		if is_equal_approx(ray.normal.dot(Vector3.UP), 1):
			impact_scene.look_at(ray.position + ray.normal, Vector3.FORWARD)
		else:
			impact_scene.look_at(ray.position + ray.normal, Vector3.UP)

		if ray.collider is Node3D:
			hits.append(ray)
			ray_params.from = ray.position
			exclude.append(ray.collider)
			ray_params.exclude = exclude
			#Logger.log(["Ray iteration %d has hit" % [iteration], ray])

		else:
			#Logger.log(["Ray iteration %d has hit nothing" % [iteration], "returning"])
			return  { "hits" : hits, "targets": ray_targets }

		if iteration == MAX_RAYCAST_ITERATIONS:
			Logger.log(["Raycast has reached the iteration limit (%d)!" % [iteration]], Logger.MessageType.ERROR)


func can_shoot() -> bool:
	return remaining_shots > 0


func check_hitscan_hit() -> Dictionary:
	var ray_spawner = character.camera if character else $Muzzle
	var from = ray_spawner.to_global(Vector3(0.0, 0.0, 0.0))
	var to = ray_spawner.to_global(Vector3(0.0, 0.0, -1000.0))

	var raycast_result = cast_ray(from, to)
	if raycast_result is Dictionary:
		return raycast_result
	else:
		return {}


func reload():
	#hide()
	if is_instance_valid(reload_tween):
		if reload_tween.is_running():
			return

	reload_tween = create_tween()

	reload_tween.tween_property(self, "rotation_degrees", Vector3(0,0,0), reload_time_seconds)\
			.from(Vector3(360,0,0)).set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CIRC)
	reload_tween.play()
	await reload_tween.finished
	#show()
	remaining_shots = magazine_size
#
#func inflict_damage():
	#pass


func recoil():
	pass

